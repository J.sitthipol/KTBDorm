package com.example.riderodead.ktbdorm.f05_Home.sf02MainContent

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.riderodead.ktbdorm.Model.SomeDorm.SomeDormDataModel
import com.example.riderodead.ktbdorm.Model.SomeDorm.SomeDormModel
import com.example.riderodead.ktbdorm.R
import com.example.riderodead.ktbdorm.adapter.ListFacilitieAdapter
import com.example.riderodead.ktbdorm.adapter.ListFacilityRoomAdapter
import com.example.riderodead.ktbdorm.f05_Home.sf04_Dorm_Room.DormRoomActivity
import kotlinx.android.synthetic.main.fragment_content_month.*

class ContentMonthFragment : Fragment(),ContentDormView {
    override fun getFacilitySuite(showFacilitySuite: String) {

    }

    override fun getFacilityShow(showFacility: String) {

    }

    override fun getFacilityShowNormal(showFacilityPosition: String) {

    }

    private val listFacility = ArrayList<String>()
    private val listFacilityRoomNormal = ArrayList<String>()
    private val listFacilityRoomSuite = ArrayList<String>()

    override fun getInformationDorm(items: SomeDormModel) {
        setDataDorm(items)
    }

    private val presenter = DormPresenter(this)
    private lateinit var data : SomeDormDataModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_content_month, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        hasSavedInstanceState(savedInstanceState)
        initInstances()
    }

    private fun hasSavedInstanceState(savedInstanceState: Bundle?) {
        if (savedInstanceState == null) {
            val sp = activity?.getSharedPreferences("PREF_POSITION", Context.MODE_PRIVATE)
            val id = sp?.getInt("ViewHolder_Position", 0)
            presenter.getInfo(id!!)
        }
    }

    private fun initInstances() {
        setDateToPage()
        onClick()
    }

    private fun onClick() {
        btn_rule.setOnClickListener {
            activity!!.supportFragmentManager.beginTransaction()
                    .add(R.id.container_main, RuleFragment())
                    .addToBackStack(null)
                    .commit()
        }
        btn_policy.setOnClickListener {
            activity!!.supportFragmentManager?.beginTransaction()
                    ?.add(R.id.container_main, PolicyFragment())
                    ?.addToBackStack(null)
                    ?.commit()
        }
        btn_rental.setOnClickListener {
            activity!!.supportFragmentManager?.beginTransaction()
                    ?.add(R.id.container_main, RentalFragment())
                    ?.addToBackStack(null)
                    ?.commit()
        }
    }

    private fun setDateToPage() {
        btn_standard_room.setOnClickListener {
            setRoomType("standard")
            goToDormRoomActivity("standard")
        }
        btn_suite_room.setOnClickListener {
            setRoomType("suite")
            goToDormRoomActivity("suite")
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setDataDorm(items: SomeDormModel) {

        data = items.data
        textview_dorm_name.text = items.data.info.title
        textView_content_location.text = data.address.addressNear
        textview_sample_name.text = data.contact.contactName
        textview_sample_phonenumber.text = data.contact.contactPhone
        textview_sameple_email.text = data.contact.contactEmail
        imageview_map.setImageResource(R.drawable.d_1_day_detail_map)
        textview_hospital.text = data.nearPlace.items[0].toString()
        textview_future.text = data.nearPlace.items[1].toString()
        textView_content_dorm.text = data.info.description
        textview_normalroom.text = getString(R.string.standard_room)
        textview_person_normalroom.text = "2 คน/ห้อง"
        textview_pricenormal.text = data.roomCategory.items[0].price.priceMonthly.toString() + getString(R.string.baht)
        textview_permonth_standard.text = getString(R.string.per_month)
        textview_stair.text = "${data.info.allFloors} " + getString(R.string.floor)
        textview_room.text = "${data.info.allRooms} " + getString(R.string.floor)
        textview_water.text = "ค่าน้ำ ${data.service.serviceWater} บาท/ยูนิต"
        textview_electric.text = "ไฟ ${data.service.serviceElectricity} บาท/ยูนิต"
        textview_mortgage.text = "มัดจำ ${data.service.serviceDeposit} เดือน"
        textview_middle_mortgage.text = "ค่าส่วนกลาง ${data.service.serviceCentral} บาท"

        val sp = activity?.getSharedPreferences("PREF_ROOM_CATEGORY", Context.MODE_PRIVATE)
        val editor = sp?.edit()
        editor?.putInt("Id", items.data.id)
        editor?.putInt("RoomCategory", data.roomCategory.items[0].roomCategoryId)
        editor?.putString("picNameDormGallery",data.dorm_gallery.items[0])
        editor?.putString("picName", data.roomCategory.items[0].room_category_gallery!!.items[0])
        editor?.apply()

        setRoomCategory()
        setRecyclerServiceView()
    }

    private fun setRecyclerServiceView() {


        addListFacility(listFacility,-1)
        recycler_services.layoutManager = GridLayoutManager(context!!,3)
        recycler_services.adapter = ListFacilitieAdapter(context!!, listFacility)

        addListFacility(listFacilityRoomNormal,0)
        recycler_services_normal_room.layoutManager = GridLayoutManager(context!!,7)
        recycler_services_normal_room.adapter = ListFacilityRoomAdapter(context!!, listFacilityRoomNormal)

        if (data.roomCategory.items.size > 1) {
            addListFacility(listFacilityRoomSuite, 1)
            recycler_services_suite_room.layoutManager = GridLayoutManager(context!!, 7)
            recycler_services_suite_room.adapter = ListFacilityRoomAdapter(context!!, listFacilityRoomSuite)
        }
    }

    private fun addListFacility(listFacility: ArrayList<String>,position : Int) {

        when(position){
            -1 -> {
                /**
                 * facilitie dorm
                 */
                if (hasFacility(data.facilities.facilitiesFitness)) listFacility.add("fitness")
                if (hasFacility(data.facilities.facilitiesElevator)) listFacility.add("elevator")
                if (hasFacility(data.facilities.facilitiesStore)) listFacility.add("shop")
                if (hasFacility(data.facilities.facilitiesCctv)) listFacility.add("cctv")
                if (hasFacility(data.facilities.facilitiesWifi)) listFacility.add("wifi")
                if (hasFacility(data.facilities.facilitiesPark)) listFacility.add("parking")
            }
            else ->{
                /**
                 * facilitie room
                 */
                if (hasFacility(data.roomCategory.items[position].facilities.facilitiesBed)) listFacility.add("bed")
                if (hasFacility(data.roomCategory.items[position].facilities.facilitiesAirCon)) listFacility.add("aircon")
                if (hasFacility(data.roomCategory.items[position].facilities.facilitiesWifi)) listFacility.add("wifi_room")
                if (hasFacility(data.roomCategory.items[position].facilities.facilitiesCableTv)) listFacility.add("cabletv")
                if (hasFacility(data.roomCategory.items[position].facilities.facilitiesTable)) listFacility.add("table")
                if (hasFacility(data.roomCategory.items[position].facilities.facilitiesFurniture)) listFacility.add("furniture")
                if (hasFacility(data.roomCategory.items[position].facilities.facilitiesWardrobe)) listFacility.add("wardrobe")
            }
        }

    }

    private fun hasFacility(facility : Int) : Boolean = facility == 1

    @SuppressLint("SetTextI18n")
    private fun setRoomCategory() {
        when {
            data.roomCategory.items.size > 2 -> {
                textview_roomsuite.text = "ห้องสวีท"
                textview_person_roomsuite.text = "2-6 คน/ห้อง"
                textview_price_roomsuite.text = data.roomCategory.items[1].price.priceMonthly.toString() + getString(R.string.baht)
                textView_permonth_suite.text = "ต่อเดือน"
            }
            else -> {
                constraint_review_roomsuite.visibility = View.GONE
                view_underline_suite.visibility = View.GONE
            }
        }
    }


    private fun setRoomType(roomType: String) {
        presenter.setAfterChoose(context!!,roomType,data)
    }

    private fun goToDormRoomActivity(roomType: String) {
        val intent = Intent(activity, DormRoomActivity::class.java)
        when (roomType) {
            "standard" -> intent.putStringArrayListExtra("listFacilityRoomType",listFacilityRoomNormal)
            "suite" -> intent.putStringArrayListExtra("listFacilityRoomType",listFacilityRoomSuite)
        }
        startActivity(intent)
    }
}
