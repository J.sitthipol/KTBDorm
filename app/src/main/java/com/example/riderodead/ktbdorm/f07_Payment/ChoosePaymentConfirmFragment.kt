package com.example.riderodead.ktbdorm.f07_Payment


import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast

import com.example.riderodead.ktbdorm.R
import kotlinx.android.synthetic.main.fragment_choose_payment_confirm.*

class ChoosePaymentConfirmFragment : Fragment(), PaymentContract {

    private var amount: String = ""

    private var paymentPresenter: PaymentPresenter? = null

    private var fee: String = "0"

    private var total: String = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_choose_payment_confirm, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        paymentPresenter = PaymentPresenter(this)
        initInstances()
    }

    @SuppressLint("SetTextI18n")
    private fun initInstances() {
        getAmountFromChooseCardFragemnt()
        fee = "10"
        sendAmountAndFee()
        textView_amount.text = amount
        btn_confirm_payment.text = "ยืนยันการโอนเงิน\n$total " + getString(R.string.baht)
        btn_confirm_payment.setOnClickListener { setDialogConfirm() }

    }

    private fun sendAmountAndFee(){
        when {
            amount == "" -> amount = "0"
            fee == "" -> fee = "0"
        }
        paymentPresenter?.calculateAmountPlusFee(amount, fee)
        textView_fee.text = fee
        textview_total.text = total
    }

    private fun getAmountFromChooseCardFragemnt(){
        val sp = activity?.getSharedPreferences("PREF_AMOUNT", Context.MODE_PRIVATE)
        amount = sp?.getString("Amount", "0.00")!!
    }

    @SuppressLint("SetTextI18n")
    private fun setDialogConfirm(){
      val dialog = Dialog(activity)
        dialog.setContentView(R.layout.layout_dialog_comfirm_payment)
        val conFirmTextView = dialog.findViewById(R.id.textView_confirm) as TextView
        val cancelTextView = dialog.findViewById(R.id.textView_cancel) as TextView
        val title = dialog.findViewById(R.id.textView_dialog_title) as TextView
        title.text = "ยืนยันการชำระเงินให้บัญชีกับบัญชี xxx-xxx160-4\nจำนวน $total บาท"
        conFirmTextView.setOnClickListener {
            dialog.dismiss()
            activity!!.supportFragmentManager.beginTransaction()
                    .add(R.id.constraint_payment,PaymentCompletedFragment())
                    .addToBackStack(null)
                    .commit()
            Toast.makeText(activity, "OK", Toast.LENGTH_SHORT).show()
        }
        cancelTextView.setOnClickListener { dialog.dismiss() }
        dialog.show()
    }

    override fun checkAlready3(already3: Boolean) {

    }

    override fun calculate(total: String) {
        this.total = total
    }

}
