package com.example.riderodead.ktbdorm.Model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ReservationDailyDataResponseModel(
        @SerializedName("message")
        val message: String)



