package com.example.riderodead.ktbdorm.Model.SomeDorm

import com.google.gson.annotations.SerializedName

data class SomeDormDataModel(
        @SerializedName("id")
        val id : Int,
        @SerializedName("info")
        val info : InfoModel,
        @SerializedName("contact")
        val contact : ContectModel,
        @SerializedName("address")
        val address : AddressModel,
        @SerializedName("service")
        val service : ServiceModel,
        @SerializedName("facilities")
        val facilities : FacilitiesModel,
        @SerializedName("dorm_gallery")
        val dorm_gallery : ItemStringModel,
        @SerializedName("room_category")
        val roomCategory : ItemRoomCategoryModel,
        @SerializedName("policy")
        val policy : ItemStringModel,
        @SerializedName("rule")
        val rule : ItemStringModel,
        @SerializedName("near_place")
        val nearPlace : ItemStringModel
)