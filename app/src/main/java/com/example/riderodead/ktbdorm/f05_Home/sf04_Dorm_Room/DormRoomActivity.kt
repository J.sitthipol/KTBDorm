package com.example.riderodead.ktbdorm.f05_Home.sf04_Dorm_Room

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.example.riderodead.ktbdorm.R

class DormRoomActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dorm_room)
        initInstances()
    }

    private fun initInstances() {
        supportFragmentManager.beginTransaction()
                .replace(R.id.content_container, DormRoomFragment())
                .commit()

    }
}
