package com.example.riderodead.ktbdorm.adapter

import com.example.riderodead.ktbdorm.HolderView.DormViewHolder
import com.example.riderodead.ktbdorm.Model.AllDorm.DormItemResponseModel
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.riderodead.ktbdorm.R

class ListDormRecyclerViewAdapter(private val context:Context,private val dormItemResponseModel: DormItemResponseModel) : RecyclerView.Adapter<DormViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DormViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.layout_list_dorm,parent,false)
        return DormViewHolder(view, dormItemResponseModel)
    }

    override fun getItemCount(): Int = dormItemResponseModel.items.size

    override fun onBindViewHolder(holder: DormViewHolder, position: Int) {
        holder.viewHolder(context,position)
    }

}