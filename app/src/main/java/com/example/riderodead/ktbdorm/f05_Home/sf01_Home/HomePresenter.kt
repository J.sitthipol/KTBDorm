package com.example.riderodead.ktbdorm.f05_Home.sf01_Home

import com.example.riderodead.ktbdorm.DormService.InfoHomeService
import com.example.riderodead.ktbdorm.DormService.SearchDormService
import com.example.riderodead.ktbdorm.Model.AllDorm.DormResponseModel
import com.example.riderodead.ktbdorm.adapter.ListDormRecyclerViewAdapter
import com.example.riderodead.ktbdorm.adapter.ListNotificationRecyclerViewAdapter
import android.content.SharedPreferences
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import kotlinx.android.synthetic.main.fragment_home.view.*
import kotlinx.android.synthetic.main.fragment_notification.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class HomePresenter(private val homeView: HomeView) {

    fun getAllDorm(view : View){
        val builder = Retrofit.Builder()
                .baseUrl("http://103.253.75.169/~cmedorm/backend/include/api/dorm/")
                .addConverterFactory(GsonConverterFactory.create())
        val retrofit = builder.build()
        val client = retrofit.create(InfoHomeService::class.java)
        val call = client.getAllDorm()
        call.enqueue(object: Callback<DormResponseModel?> {
            override fun onFailure(call: Call<DormResponseModel?>?, t: Throwable?) {

            }

            override fun onResponse(call: Call<DormResponseModel?>?, responseModel: Response<DormResponseModel?>?) {
                val items = responseModel?.body()
                homeView.getListDorm(items!!)
                view.recyclerView_home.layoutManager = LinearLayoutManager(view.context)
                view.recyclerView_home.adapter = ListDormRecyclerViewAdapter(view.context!!, items.data)
            }
        })

    }

    fun getNotification(view: View){
            view.recyclerview_noti.layoutManager = LinearLayoutManager(view.context)
            view.recyclerview_noti.adapter = ListNotificationRecyclerViewAdapter(view.context)
    }

    fun getSearchDorm(view: View, sp: SharedPreferences){
        val builder = Retrofit.Builder()
                .baseUrl("http://103.253.75.169/~cmedorm/backend/include/api/dorm/")
                .addConverterFactory(GsonConverterFactory.create())
        val retrofit = builder.build()
        val client = retrofit.create(SearchDormService::class.java)
        val call = client.getSearchDorm(sp!!.getString("Search_Name",""))
        call.enqueue(object: Callback<DormResponseModel?> {
            override fun onFailure(call: Call<DormResponseModel?>?, t: Throwable?) {

            }

            override fun onResponse(call: Call<DormResponseModel?>?, responseModel: Response<DormResponseModel?>?) {
                val items = responseModel?.body()
                homeView.getListDorm(items!!)
                view.recyclerView_home.layoutManager = LinearLayoutManager(view.context)
                view.recyclerView_home.adapter = ListDormRecyclerViewAdapter(view.context!!, items.data)
            }
        })
    }
}