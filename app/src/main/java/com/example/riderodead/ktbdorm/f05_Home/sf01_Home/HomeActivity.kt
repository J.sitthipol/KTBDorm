package com.example.riderodead.ktbdorm.f05_Home.sf01_Home

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.example.riderodead.ktbdorm.R
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity() {

    private var status = "HOME"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        initInstances()
    }
    private fun initInstances() {
        attachHome()
        btnClick()
    }

    private fun attachHome() {
        supportFragmentManager.beginTransaction()
                .replace(R.id.home_content_container, HomeFragment())
                .commit()
    }

    private fun btnClick() {

        toolbar_notification.setOnClickListener {
            if (status != "NOTI") {
                supportFragmentManager.beginTransaction()
                        .replace(R.id.home_content_container, NotificationFragment())
                        .commit()
                status = "NOTI"
                toolbar_profile.setImageResource(R.drawable.ic_profile)
                toolbar_notification.setImageResource(R.drawable.ic_noti_select)
                toolbar_home.setImageResource(R.drawable.ic_no_select_home)
            }
        }


        toolbar_home.setOnClickListener {
            if (status != "HOME")
            supportFragmentManager.beginTransaction()
                    .replace(R.id.home_content_container, HomeFragment())
                    .commit()
            status = "HOME"
            toolbar_profile.setImageResource(R.drawable.ic_profile)
            toolbar_home.setImageResource(R.drawable.ic_home)
            toolbar_notification.setImageResource(R.drawable.ic_no_select_noti)
        }

        toolbar_profile.setOnClickListener {
            if (status != "PROFILE")
                supportFragmentManager.beginTransaction()
                        .replace(R.id.home_content_container, ProfileFragment())
                        .commit()
            status = "PROFILE"
            toolbar_profile.setImageResource(R.drawable.ic_select_user)
            toolbar_home.setImageResource(R.drawable.ic_no_select_home)
            toolbar_notification.setImageResource(R.drawable.ic_no_select_noti)
        }
    }


}