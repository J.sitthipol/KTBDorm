package com.example.riderodead.ktbdorm.f05_Home.sf05_Reserved_Room

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.riderodead.ktbdorm.R
import kotlinx.android.synthetic.main.fragment_reserved.*
import java.util.*

class ReservedFragment : Fragment(), ReservationContract {

    private var reservationPresenter: ReservationPresenter? = null

    private var disposeOK: Unit? = null

    private var dateStart: String? = null

    private var dateEnd: String? = null

    private var roomCategory: Int? = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_reserved, container, false)
    }

    private lateinit var uniqueId: String

    private var rentType: String? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val reservedPref = activity?.getSharedPreferences("PREF_RESERVED", Context.MODE_PRIVATE)
        val roomTypePref = activity?.getSharedPreferences("PREF_ROOM_TYPE", Context.MODE_PRIVATE)
        val rentTypePref = activity?.getSharedPreferences("PREF_RENT_TYPE", Context.MODE_PRIVATE)
        val roomCategoryPref = activity?.getSharedPreferences("PREF_ROOM_CATEGORY", Context.MODE_PRIVATE)
        reservationPresenter = ReservationPresenter(this)
        initInstance(reservedPref!!, roomTypePref!!)
        dateStart = reservedPref.getString("DateStartFormat", "N/A")
        dateEnd = reservedPref.getString("DateEndFormat", "N/A")
        roomCategory = roomCategoryPref!!.getInt("RoomCategory", 1)
        btnClickDone(dateStart!!, dateEnd!!, rentTypePref, roomCategory!!)
    }

    @SuppressLint("SetTextI18n")
    private fun initInstance(reservedPref: SharedPreferences, roomTypePref: SharedPreferences) {
        textview_content_checkin.text = reservedPref.getString("checkIn", "Error!!")
        textview_content_checkout.text = reservedPref.getString("checkOut", "Error!!")
        textview_room_type.text = setRentType(roomTypePref)
        textview_price_day.text = "${roomTypePref.getInt("Room_Price", 1)} " + getString(R.string.baht)
        textview_duration.text = "${reservedPref.getLong("Duration", 0)} " + getString(R.string.day)
        textview_content_night.text = "${reservedPref.getLong("Duration", 0)} " + getString(R.string.night)
        textview_total_price.text = "${(reservedPref.getLong("Duration", 0) * roomTypePref.getInt("Room_Price", 1))}"
    }

    private fun setRentType(roomTypePref: SharedPreferences): CharSequence? {
        return when (roomTypePref.getString("Room_Type", "Error!!")) {
            "standard" -> "ห้องธรรมดา"
            "suite" -> "ห้องสวีท"
            else -> {
                "N/A"
            }
        }
    }

    private fun btnClickDone(dateStart: String, dateEnd: String, rentTypePref: SharedPreferences?, roomCategory: Int) {
        uniqueId = UUID.randomUUID().toString()
        rentType = rentTypePref?.getString("Rent_Type", "daily")
        Log.v("Benz", "$uniqueId : $dateStart : $dateEnd : $rentType : $roomCategory")
        button_confirm.setOnClickListener {
            when (rentType) {
                "daily" -> sendDataToService(uniqueId, roomCategory, rentType!!, dateStart, dateEnd)
                "month" -> sendDataToServiceMonth(uniqueId,roomCategory,rentType!!)
            }

        }
    }

    private fun sendDataToService(uniqueId: String, roomCategoryId: Int, reservationCategory: String, dateStart: String, dateEnd: String) {
        reservationPresenter?.setUpService(uniqueId, roomCategoryId, reservationCategory, dateStart, dateEnd)
    }

    private fun sendDataToServiceMonth(uniqueId: String, roomCategoryId: Int, reservationCategory: String) {
        reservationPresenter?.setUpMonthService(uniqueId,roomCategoryId,reservationCategory)
    }

    override fun serviceOK(status: Boolean) {
        when {
            status -> {
                Log.v("Benz","status daily : $status")
                goToCompleteReservation()
            }
        }
    }

    override fun onPause() {
        super.onPause()
        reservationPresenter?.disposable()
        disposeOK
    }

    private fun goToCompleteReservation() {
        activity!!.supportFragmentManager.beginTransaction()
                .replace(R.id.content_container, ReservationCompletedFragment())
                .commit()
    }

    override fun disposableOK(dispose: Unit?) {
        disposeOK = dispose
    }

    override fun serviceMonthOK(status: Boolean) {
        when {
            status -> {
                Log.v("Benz","status month : $status")
                goToCompleteReservation()
            }
        }
    }


}