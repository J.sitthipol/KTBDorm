package com.example.riderodead.ktbdorm.f04_ForgotPassword

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.riderodead.ktbdorm.R
import kotlinx.android.synthetic.main.activity_forgot_password.*

class ForgotPasswordActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password)
        ininstance()
    }

    private fun ininstance() {
        supportFragmentManager.beginTransaction()
                .replace(R.id.forgot_content_container, ForgotPasswordMainFragment())
                .commit()

        back.setOnClickListener { onBackPressed() }
    }
}
