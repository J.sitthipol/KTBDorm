package com.example.riderodead.ktbdorm.adapter

import android.content.Context
import android.support.v4.app.FragmentActivity
import android.support.v4.view.PagerAdapter
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.example.riderodead.ktbdorm.R
import com.example.riderodead.ktbdorm.f05_Home.DormMainActivity
import com.example.riderodead.ktbdorm.f05_Home.FragmentFullScreen
import kotlinx.android.synthetic.main.layout_slidingimages.view.*

class SlidingImageAdapter(private var dormMainActivity: DormMainActivity) : PagerAdapter() {

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val layout = LayoutInflater.from(container.context)
                .inflate(R.layout.layout_slidingimages, container, false) as ViewGroup
        setImage(layout)
        onClick(layout, container, position)
        container.addView(layout)
        return layout
    }

    private fun setImage(layout: ViewGroup) {
        val sp = dormMainActivity.getSharedPreferences("PREF_ROOM_CATEGORY", Context.MODE_PRIVATE)
        val dormId = sp?.getInt("Id", 2)
        val pictureName = sp?.getString("picNameDormGallery", "dorm1.png")
        Log.v("Benz", "$dormId/$pictureName")
        Glide.with(dormMainActivity)
                .load("http://103.253.75.169/~cmedorm/backend/assets/dorm_gallery/$dormId/$pictureName")
                .apply(RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(layout.image_review)
    }


    private fun onClick(layout: ViewGroup, container: ViewGroup, position: Int) {
        layout.image_review.setOnClickListener {
            val sp = container.context.getSharedPreferences("PREF_TYPE_RENT", Context.MODE_PRIVATE)
            val editor = sp.edit()
            editor.putInt("positionImage", position)
            editor.apply()
            (container.context as FragmentActivity).supportFragmentManager.beginTransaction()
                    .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left,
                            R.anim.enter_from_left, R.anim.exit_to_right)
                    .add(R.id.container_main, FragmentFullScreen())
                    .addToBackStack(null)
                    .commit()
        }
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View?)
    }

    override fun getItemPosition(`object`: Any): Int {
        return PagerAdapter.POSITION_NONE
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return position.toString()
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun getCount(): Int = 1

}
