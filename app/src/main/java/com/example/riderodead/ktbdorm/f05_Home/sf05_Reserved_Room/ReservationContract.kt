package com.example.riderodead.ktbdorm.f05_Home.sf05_Reserved_Room

interface ReservationContract {
    fun serviceOK(status: Boolean)
    fun serviceMonthOK(status: Boolean)
    fun disposableOK(dispose: Unit?)
}