package com.example.riderodead.ktbdorm

import android.os.Bundle
import android.os.CountDownTimer
import android.support.v7.app.AppCompatActivity
import com.example.riderodead.ktbdorm.f01_SplashScreen.SplashScreenFragment
import com.example.riderodead.ktbdorm.f02_Login.BeforeLoginFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initInstances()
    }


    private fun initInstances() {
        attachSplashScreen()
        splashScreenTimeOut()
    }

    private fun splashScreenTimeOut() {
        object : CountDownTimer(3000,1000) {
            override fun onFinish() {
                attachBeforeLoginScreen()
            }

            override fun onTick(p0: Long) {

            }

        }.start()
    }

    private fun attachSplashScreen() {
        supportFragmentManager.beginTransaction()
                .replace(R.id.content_container, SplashScreenFragment())
                .commit()
    }

    private fun attachBeforeLoginScreen() {
        supportFragmentManager.beginTransaction()
                .replace(R.id.content_container, BeforeLoginFragment())
                 .commit()
    }
}
