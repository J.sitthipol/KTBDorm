package com.example.riderodead.ktbdorm.Model.SomeDorm

import com.google.gson.annotations.SerializedName

data class ServiceModel(
        @SerializedName("service_water")
        val serviceWater: Int,
        @SerializedName("service_electricity")
        val serviceElectricity: Int,
        @SerializedName("service_deposit")
        val serviceDeposit: Int,
        @SerializedName("service_central")
        val serviceCentral: Int
)