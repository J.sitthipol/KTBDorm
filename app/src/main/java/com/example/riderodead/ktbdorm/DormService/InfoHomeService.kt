package com.example.riderodead.ktbdorm.DormService

import com.example.riderodead.ktbdorm.Model.AllDorm.DormResponseModel
import com.example.riderodead.ktbdorm.Model.SomeDorm.SomeDormModel
import retrofit2.Call
import retrofit2.http.*

interface InfoHomeService {

    @GET("DormGetAll.php")
    fun getAllDorm() : Call<DormResponseModel>

    @FormUrlEncoded
    @POST("DormGet.php")
    fun getOnceDorm(@Field("dorm_id") dorm_id : Int) : Call<SomeDormModel>
}