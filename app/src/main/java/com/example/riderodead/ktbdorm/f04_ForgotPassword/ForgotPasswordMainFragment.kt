package com.example.riderodead.ktbdorm.f04_ForgotPassword


import android.app.Activity
import android.content.Context
import android.graphics.PorterDuff
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import com.example.riderodead.ktbdorm.R
import kotlinx.android.synthetic.main.fragment_forgot_password_main.*

class ForgotPasswordMainFragment : Fragment() {

    private var mContext: Context? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_forgot_password_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ininstance(view)
    }

    private fun ininstance(view: View) {
        email_error.visibility = View.GONE
        edittext_email_forgot.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(forgot: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (!forgot.isNullOrBlank()) {
                    if (!isEmailValid(edittext_email_forgot.text)) {
                        edittext_email_forgot.background.setColorFilter(ContextCompat.getColor(mContext!!, R.color.colorRed), PorterDuff.Mode.SRC_IN)
                        email_error.text = "รูปแบบอีเมลไม่ถูกต้อง "
                        email_error.visibility = View.VISIBLE
                        button_reset_password.isEnabled = false
                    } else {
                        edittext_email_forgot.background.setColorFilter(ContextCompat.getColor(mContext!!, R.color.colorLineGray), PorterDuff.Mode.SRC_IN)
                        email_error.visibility = View.GONE
                        button_reset_password.isEnabled = true
                    }

                }
            }

        })
        button_reset_password.setOnClickListener {
            if (isEmailValid(edittext_email_forgot.text.toString())) {
                edittext_email_forgot.background.setColorFilter(ContextCompat.getColor(mContext!!, R.color.colorLineGray), PorterDuff.Mode.SRC_IN)
                email_error.visibility = View.GONE
                goToResetComplete()
            } else {
                email_error.text = "รูปแบบอีเมลไม่ถูกต้อง"
                edittext_email_forgot.background.setColorFilter(ContextCompat.getColor(mContext!!, R.color.colorRed), PorterDuff.Mode.SRC_IN)
                email_error.visibility = View.VISIBLE
            }
        }
        view.setOnTouchListener { keyboardHide, _ ->
            hideKeyboard(keyboardHide)
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        mContext = context
    }

    private fun hideKeyboard(view: View): Boolean {
        val imm = activity?.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        return imm.hideSoftInputFromWindow(view.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)

    }

    private fun isEmailValid(email: CharSequence): Boolean {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    private fun goToResetComplete() {
        activity?.supportFragmentManager?.beginTransaction()
                ?.replace(R.id.forgot_content_container, ForgotPasswordCompleteFragment())
                ?.commit()
    }
}
