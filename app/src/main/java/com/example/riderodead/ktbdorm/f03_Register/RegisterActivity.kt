package com.example.riderodead.ktbdorm.f03_Register

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.example.riderodead.ktbdorm.R
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        initInstances()
    }

    private fun initInstances() {
        attachRegister()
        back.setOnClickListener { onBackPressed() }
    }

    private fun attachRegister() {
        supportFragmentManager.beginTransaction()
                .replace(R.id.register_content_container, RegisterFragment())
                .commit()
    }



}
