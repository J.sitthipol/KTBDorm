package com.example.riderodead.ktbdorm.f05_Home.sf05_Reserved_Room

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.example.riderodead.ktbdorm.R

class ReservationActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reservation)
        initInstance()
    }

    private fun initInstance() {
        supportFragmentManager.beginTransaction()
                .replace(R.id.content_container, ReservedFragment())
                .commit()
    }
}
