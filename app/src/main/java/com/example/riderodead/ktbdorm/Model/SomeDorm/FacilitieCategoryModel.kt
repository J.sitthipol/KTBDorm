package com.example.riderodead.ktbdorm.Model.SomeDorm

import com.google.gson.annotations.SerializedName

data class FacilitieCategoryModel(
        @SerializedName("facilities_bed")
        val facilitiesBed: Int,
        @SerializedName("facilities_air_con")
        val facilitiesAirCon: Int,
        @SerializedName("facilities_wifi")
        val facilitiesWifi: Int,
        @SerializedName("facilities_cable_tv")
        val facilitiesCableTv: Int,
        @SerializedName("facilities_table")
        val facilitiesTable: Int,
        @SerializedName("facilities_furniture")
        val facilitiesFurniture: Int,
        @SerializedName("facilities_wardrobe")
        val facilitiesWardrobe: Int
)