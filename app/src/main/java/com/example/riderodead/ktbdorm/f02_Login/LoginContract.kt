package com.example.riderodead.ktbdorm.f02_Login

interface LoginContract {
    fun serviceOK(status: Boolean)
    fun checkEmail(emailChecked: Boolean)
    fun checkNull(nullChecked: Boolean)
}