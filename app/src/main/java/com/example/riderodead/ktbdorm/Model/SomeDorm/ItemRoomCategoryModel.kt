package com.example.riderodead.ktbdorm.Model.SomeDorm

import com.google.gson.annotations.SerializedName

data class ItemRoomCategoryModel(
        @SerializedName("items")
        val items: ArrayList<ItemCategoryModel>

)