package com.example.riderodead.ktbdorm.f05_Home.sf05_Reserved_Room

import com.example.riderodead.ktbdorm.DormService.ReservationDailyService
import com.example.riderodead.ktbdorm.DormService.ReservationMonthService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class ReservationPresenter(private val resevationContract: ReservationContract) {
    private val client by lazy {
        ReservationDailyService.crate()
    }

    private val clientMonth by lazy {
        ReservationMonthService.crate()
    }

    private var disposable: Disposable? = null

    private var disposableMonth: Disposable? = null

    fun setUpService(uniqueId: String, roomCategoryId: Int, reservationCategory: String, dateStart: String, dateEnd: String) {
        disposable = client.serviceReservation(uniqueId,
                roomCategoryId,
                reservationCategory,
                dateStart,
                dateEnd)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ it -> resevationContract.serviceOK(it.status) },
                        { error -> error.message })
    }

    fun setUpMonthService(uniqueId: String, roomCategoryId: Int, reservationCategory: String) {
        disposableMonth = clientMonth.serviceReservationMonth(uniqueId,
                roomCategoryId,
                reservationCategory)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({it -> resevationContract.serviceMonthOK(it.status)},
                        { error -> error.message})
    }

    fun disposable() {
        resevationContract.disposableOK(disposable?.dispose())
    }
}