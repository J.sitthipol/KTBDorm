package com.example.riderodead.ktbdorm.f05_Home.sf01_Home

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.riderodead.ktbdorm.Model.AllDorm.DormDataResponseModel
import com.example.riderodead.ktbdorm.Model.AllDorm.DormResponseModel
import com.example.riderodead.ktbdorm.R

class NotificationFragment : Fragment(),HomeView{

    private var mContext: Context? = null
    private var presenter = HomePresenter(this)

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        mContext = context
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_notification,container,false)
        presenter.getNotification(rootView)
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initInstances()
    }

    private fun initInstances() {

    }

    override fun getListDorm(listDormModel: DormResponseModel) {

    }

    override fun searchDorm(Dorm: DormDataResponseModel) {

    }
}