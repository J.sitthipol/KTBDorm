package com.example.riderodead.ktbdorm.HolderView

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.widget.ImageView
import com.example.riderodead.ktbdorm.R
import kotlinx.android.synthetic.main.layout_service.view.*

class FacilitieViewHolder(val view: View) : RecyclerView.ViewHolder(view){

    private var mContext : Context? = null

    fun viewHolder(context: Context, position : Int) {
        mContext = context
    }

    fun setView(view:View,facility : String) : View {
        when{
            facility.equals("fitness") -> {
                view.imageview_icon.setImageResource(R.drawable.ic_fitness)
                view.textview_services.text = "ฟิตเนส"
            }
            facility.equals("elevator") -> {
                view.imageview_icon.setImageResource(R.drawable.ic_elevator)
                view.textview_services.text = "ลิฟท์"
            }
            facility.equals("shop") -> {
                view.imageview_icon.setImageResource(R.drawable.ic_shop)
                view.textview_services.text = "ร้านสะดวกซื้อ"
            }
            facility.equals("cctv") -> {
                view.imageview_icon.setImageResource(R.drawable.ic_cctv)
                view.textview_services.text = "กล้องวงจรปิด"
            }
            facility.equals("wifi") -> {
                view.imageview_icon.setImageResource(R.drawable.ic_wifi)
                view.textview_services.text = "WI-FI"
            }
            facility.equals("parking") -> {
                view.imageview_icon.setImageResource(R.drawable.ic_parking)
                view.textview_services.text = "ลานจอดรถ"
            }
            facility.equals("bed") -> {
                view.imageview_icon.setImageResource(R.drawable.icon_bed)
            }
            facility.equals("aircon") -> {
                view.imageview_icon.setImageResource(R.drawable.icon_air_conditioner)
            }
            facility.equals("wifi_room") -> {
                view.imageview_icon.setImageResource(R.drawable.ic_wifi)
            }
            facility.equals("cabletv") -> {
                view.imageview_icon.setImageResource(R.drawable.television)
            }
            facility.equals("table") -> {
                view.imageview_icon.setImageResource(R.drawable.icon_desk)
            }
            facility.equals("furniture") -> {
                view.imageview_icon.setImageResource(R.drawable.armchair)
            }
            facility.equals("wardrobe") -> {
                view.imageview_icon.setImageResource(R.drawable.icon_closet)
            }
        }
        return view
    }

}