package com.example.riderodead.ktbdorm.DatePicker


import android.app.DatePickerDialog
import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import java.util.*

class MyDatePickerFragment : DialogFragment(){

    private var onDateSendListener: SendDateListener? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val calendar = Calendar.getInstance()
        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH) + 1
        val day = calendar.get(Calendar.DAY_OF_MONTH)
        return DatePickerDialog(activity,dateSetListener,year,month,day)
    }

    private val dateSetListener = DatePickerDialog.OnDateSetListener { _, year, month, day ->
        onDateSendListener?.onSendData(day,month,year)
    }

    interface SendDateListener{
        fun onSendData(day: Int, month: Int, year: Int)
    }

    fun setOnSendDate(onDateSendListener: SendDateListener) {
        this.onDateSendListener = onDateSendListener
    }

}
