package com.example.riderodead.ktbdorm.f08_QRCodePayment

import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager
import android.os.Build
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat

class QRCodePresenter(private val qrCodeView: QRCodeView){

    private var mActivity : Activity? = null

    fun setContext(activity: Activity){
        mActivity = activity
    }

    fun checkCameraApiVersion() {
        val currentApiVersion = Build.VERSION.SDK_INT
        if (currentApiVersion >= 21) {
            if (checkPermission()) {
                qrCodeView.checkApi(true)
            } else {
                requestPermission()
                qrCodeView.checkApi(true)
            }
        }
    }

    private fun checkPermission() : Boolean {
        return (ContextCompat.checkSelfPermission(mActivity!!,android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED)
    }

    private fun requestPermission(){
        ActivityCompat.requestPermissions(mActivity!!, arrayOf(Manifest.permission.CAMERA),1)
    }
}