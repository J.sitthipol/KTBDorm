package com.example.riderodead.ktbdorm.f05_Home.sf06_BeforePayment

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.example.riderodead.ktbdorm.R
import com.example.riderodead.ktbdorm.f06_OnlinePayment.OnlinePaymentActivity
import com.example.riderodead.ktbdorm.f08_QRCodePayment.QRCodeActivity
import kotlinx.android.synthetic.main.activity_before_payment.*

class BeforePaymentActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_before_payment)
        initInstances()
    }

    private fun initInstances() {
        onClick()
    }

    private fun onClick() {
        back.setOnClickListener { onBackPressed() }
        framelayout_button_qr.setOnClickListener {
            val intent = Intent(this,QRCodeActivity::class.java)
            startActivity(intent)
        }
        framelayout_button_onlinepay.setOnClickListener {
            val intent = Intent(this,OnlinePaymentActivity::class.java)
            startActivity(intent)
        }
    }

}
