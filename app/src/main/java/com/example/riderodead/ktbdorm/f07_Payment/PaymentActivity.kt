package com.example.riderodead.ktbdorm.f07_Payment

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.example.riderodead.ktbdorm.R
import kotlinx.android.synthetic.main.activity_payment.*
import kotlinx.android.synthetic.main.fragment_payment_choose_card.*

class PaymentActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment)
        initInstances()
    }

    private fun initInstances() {
        goToPaymentFragment()
        back.setOnClickListener { onBackPressed() }

        setSharePreferenceFromAddCard()
    }

    private fun setSharePreferenceFromAddCard() {
        if (intent.getStringExtra("CaditNumber") != null && intent.getStringExtra("CaditDate") != null) {
            val caditName = intent.extras["CaditNumber"].toString()
            val caditDate = intent.extras["CaditDate"].toString()
            val sharePreferenceResultCard = this.getSharedPreferences("PREF_RESULTCARD", Context.MODE_PRIVATE)
            val editor = sharePreferenceResultCard.edit()
            editor.putString("CaditName", caditName)
            editor.putString("CaditDate", caditDate)
            editor.putInt("RequestCode", 1)
            editor.commit()
        }
    }

    private fun goToPaymentFragment() {
        supportFragmentManager.beginTransaction()
                .replace(R.id.content_container, PaymentChooseCardFragment())
                .commit()
    }
}
