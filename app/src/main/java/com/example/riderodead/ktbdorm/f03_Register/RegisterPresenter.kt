package com.example.riderodead.ktbdorm.f03_Register

import android.text.Editable
import android.text.SpannableStringBuilder
import android.util.Log
import com.example.riderodead.ktbdorm.DormService.RegisterService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class RegisterPresenter(private val registerContract: RegisterContract) {

    private val client by lazy {
        RegisterService.crate()
    }

    private var disposable: Disposable? = null

    fun isEmailValid(email: CharSequence) {
        registerContract.checkEmail(android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches())
    }

    fun setIdCardFormat(idCard: Editable?): SpannableStringBuilder {
        return SpannableStringBuilder(idCard!!.substring(0, 1) + "-" + idCard.substring(2, 6) + "-" + idCard.substring(5, 10) + "-" + idCard.substring(10, 12) + "-" + idCard.substring(12, 13))
    }

    fun setPhoneFormat(phone: Editable): SpannableStringBuilder {
        return SpannableStringBuilder(phone.substring(0, 3) + "-" + phone.substring(3, 6) + "-" + phone.substring(6, 10))
    }

    fun checkPassword(password: String) {
        when {
            password.length < 8 || password.isEmpty() -> {
                registerContract.checkPassword(false)
            }
            password.length >= 8 -> {
                registerContract.checkPassword(true)
            }
        }
    }
    fun setUpServiceRegister(email: String, password: String, fullName: String, address: String, idCard: String, phone: String, birthDay: String) {
        disposable = client.addService(email,
                password,
                fullName,
                address,
                idCard,
                phone,
                birthDay)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result ->
                            registerContract.servicesOK(result.status)
                        },
                        { error -> Log.e("ERROR", error.message) }
                )
    }

    fun allCorrect(emailChecked: Boolean, passwordChecked: Boolean, idCardChecked: Boolean, addressChecked: Boolean, nameChecked: Boolean, rePasswordChecked: Boolean, checkTermClicked: Boolean) {
        when {
            emailChecked && passwordChecked
                    && idCardChecked
                    && addressChecked
                    && nameChecked
                    && rePasswordChecked
                    && checkTermClicked
            -> registerContract.checkAllField(true)
            else -> registerContract.checkAllField(false)
        }
    }

    fun disposable(): Unit? {
        return disposable?.dispose()
    }


}