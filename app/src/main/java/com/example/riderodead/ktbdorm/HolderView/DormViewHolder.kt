package com.example.riderodead.ktbdorm.HolderView

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.View
import com.example.riderodead.ktbdorm.Model.AllDorm.DormItemResponseModel
import com.example.riderodead.ktbdorm.f05_Home.DormMainActivity
import kotlinx.android.synthetic.main.layout_list_dorm.view.*

class DormViewHolder(val view: View, private val dormItemResponseModel : DormItemResponseModel) : RecyclerView.ViewHolder(view) {

    private var mContext : Context? = null
    fun viewHolder(context: Context,position : Int) {
        mContext = context
        init(position)
    }

    private fun init(position : Int) {
        setView(position)
        view.viewDetail.setOnClickListener {
            setSharedPrefetence(mContext!!, dormItemResponseModel.items[position].dormId)
            goToDormActivity()
        }
    }

    private fun setView(position: Int) {
        view.textView_dorm.text = dormItemResponseModel.items[position].title
        view.textView_phoneNumber.text = dormItemResponseModel.items[position].contactPhone
        view.textView_location.text = dormItemResponseModel.items[position].address
        view.textView_priceofmonth.text = setPriceDay(position)
        view.textView_priceofday.text = setPriceMonth(position)

        when {
            isNotEmptyRoom(position) -> view.textView_available.text = dormItemResponseModel.items[position].remainingRoom.toString()
            else -> {
                view.textView_empty.visibility = View.INVISIBLE
                view.textView_room.text = "เต็ม"
            }
        }
    }

    private fun isNotEmptyRoom(position: Int) = dormItemResponseModel.items[position].remainingRoom != 0

    private fun setPriceMonth(position: Int): String {
        return dormItemResponseModel.items[position]
                .roomCategory
                .intDailyModel
                .items[0].toString() +
                when {
                    isPriceRange(position) -> " - " + dormItemResponseModel.items[position]
                            .roomCategory
                            .intDailyModel
                            .items[1].toString() + " บาท"
                    else -> " บาท"
                }
    }

    private fun setPriceDay(position: Int): String {
        return dormItemResponseModel.items[position]
                .roomCategory
                .intMonthlyModel
                .items[0].toString() +
                when {
                    isPriceRange(position) -> " - " + dormItemResponseModel.items[position]
                            .roomCategory
                            .intMonthlyModel
                            .items[1].toString() + " บาท"
                    else -> " บาท"
                }
    }

    private fun isPriceRange(position: Int) = dormItemResponseModel.items[position].roomCategory.intMonthlyModel.items.size > 1

    private fun goToDormActivity() {
        val intent = Intent(view.context, DormMainActivity::class.java)
        view.context.startActivity(intent)
    }

    private fun setSharedPrefetence(context: Context, adapterPosition: Int) {
        val sp = context.getSharedPreferences("PREF_POSITION", Context.MODE_PRIVATE)
        val editor = sp.edit()
        editor.putInt("ViewHolder_Position", adapterPosition)
        editor.apply()
    }


}