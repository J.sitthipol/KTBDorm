package com.example.riderodead.ktbdorm.DormService

import com.example.riderodead.ktbdorm.Model.AllDorm.DormResponseModel
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface SearchDormService {

    @FormUrlEncoded
    @POST("DormGet.php")
    fun getSearchDorm(@Field("title") title : String) : Call<DormResponseModel>
}