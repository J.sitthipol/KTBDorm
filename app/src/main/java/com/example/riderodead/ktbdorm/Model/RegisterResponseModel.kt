package com.example.riderodead.ktbdorm.Model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class RegisterResponseModel(
        @SerializedName("status")
        @Expose
        var status: Boolean,
        @SerializedName("data")
        @Expose
        var data: RegisterDataResponseModel
)