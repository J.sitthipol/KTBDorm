package com.example.riderodead.ktbdorm.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.riderodead.ktbdorm.HolderView.FacilityNormalSuiteViewHolder
import com.example.riderodead.ktbdorm.R

class ListFacilityRoomNormalSuiteAdapter(private val context: Context, private val listFacilityRoomType: ArrayList<String>)
    : RecyclerView.Adapter<FacilityNormalSuiteViewHolder>(){

    private var count = -1
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FacilityNormalSuiteViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.layout_service_normal_suite_room,parent,false)
        count++
        val facility = FacilityNormalSuiteViewHolder(view)
        facility.setView(view,listFacilityRoomType[count])
        return facility
    }

    override fun getItemCount(): Int = listFacilityRoomType.size

    override fun onBindViewHolder(holder: FacilityNormalSuiteViewHolder, position: Int) {
        holder.viewHolder(context)
    }

}