package com.example.riderodead.ktbdorm.f07_Payment

import com.example.riderodead.ktbdorm.Model.CreditCardModel
import android.util.Log
import com.example.riderodead.ktbdorm.f07_Payment.Adapter.ViewPagerAdapter

class PaymentPresenter(private val paymentContract: PaymentContract){

    fun checkModelAlready3(creditCardList: ArrayList<CreditCardModel>) {
        when {
            creditCardList.size == 3 -> {
                paymentContract.checkAlready3(true)
            }
            creditCardList.size < 3 -> {
                paymentContract.checkAlready3(false)
            }
        }
        Log.v("Benz","total : $paymentContract")
    }

    fun calculateAmountPlusFee(amount: String, fee: String) {
        val amountInt = amount.toInt()
        val feeInt = fee.toInt()
        val total = amountInt + feeInt
        paymentContract.calculate(total.toString())
    }
}