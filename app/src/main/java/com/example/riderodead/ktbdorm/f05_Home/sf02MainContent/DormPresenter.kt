package com.example.riderodead.ktbdorm.f05_Home.sf02MainContent

import android.content.Context
import android.util.Log
import com.example.riderodead.ktbdorm.DormService.InfoHomeService
import com.example.riderodead.ktbdorm.Model.SomeDorm.FacilitiesModel
import com.example.riderodead.ktbdorm.Model.SomeDorm.ItemRoomCategoryModel
import com.example.riderodead.ktbdorm.Model.SomeDorm.SomeDormDataModel
import com.example.riderodead.ktbdorm.Model.SomeDorm.SomeDormModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class DormPresenter(val contentDormView: ContentDormView) {

    fun getInfo(id: Int) {
        val builder = Retrofit.Builder()
                .baseUrl("http://103.253.75.169/~cmedorm/backend/include/api/dorm/")
                .addConverterFactory(GsonConverterFactory.create())
        val retrofit = builder.build()
        val client = retrofit.create(InfoHomeService::class.java)
        val call = client.getOnceDorm(id)
        call.enqueue(object : Callback<SomeDormModel> {
            override fun onFailure(call: Call<SomeDormModel?>?, t: Throwable?) {
                Log.e("t", t.toString())
            }

            override fun onResponse(call: Call<SomeDormModel?>?, response: Response<SomeDormModel?>?) {
                val items = response?.body()
                contentDormView.getInformationDorm(items!!)
            }
        })

    }

    fun setAfterChoose(context: Context, roomType: String, data: SomeDormDataModel) {
        val sharedRoomType = context.getSharedPreferences("PREF_ROOM_TYPE", Context.MODE_PRIVATE)
        val sharedRentType = context.getSharedPreferences("PREF_RENT_TYPE", Context.MODE_PRIVATE)
        val rentType = sharedRentType.getString("Rent_Type", "daily")
        val editor = sharedRoomType?.edit()

        when (roomType) {
            "standard" -> {
                editor?.putString("Room_Type", roomType)
                editor?.putString("Room_Title", data.roomCategory.items[0].info.title)
                when {
                    isRentDay(rentType) -> editor?.putInt("Room_Price", data.roomCategory.items[0].price.priceDaily)
                    else -> editor?.putInt("Room_Price", data.roomCategory.items[0].price.priceMonthly)
                }
                editor?.putInt("Room_Size", data.roomCategory.items[0].info.size)
                editor?.putInt("Room_Floor_Start", data.roomCategory.items[0].floor.floorStart)
                editor?.putInt("Room_Floor_End", data.roomCategory.items[0].floor.floorEnd)
            }
            "suite" -> {
                editor?.putString("Room_Type", roomType)
                editor?.putString("Room_Title", data.roomCategory.items[1].info.title)
                when {
                    isRentDay(rentType) -> editor?.putInt("Room_Price", data.roomCategory.items[1].price.priceDaily)
                    else -> editor?.putInt("Room_Price", data.roomCategory.items[1].price.priceMonthly)
                }
                editor?.putInt("Room_Size", data.roomCategory.items[1].info.size)
                editor?.putInt("Room_Floor_Start", data.roomCategory.items[1].floor.floorStart)
                editor?.putInt("Room_Floor_End", data.roomCategory.items[1].floor.floorEnd)
            }
        }
        editor?.apply()
    }

    private fun isRentDay(type: String): Boolean = type == "daily"


    fun hasFacility(facility: FacilitiesModel) {
        when {facility.facilitiesFitness == 1 -> contentDormView.getFacilityShow("fitness") }
        when {facility.facilitiesElevator == 1 -> contentDormView.getFacilityShow("elevator") }
        when {facility.facilitiesStore == 1 -> contentDormView.getFacilityShow("store") }
        when {facility.facilitiesCctv == 1 -> contentDormView.getFacilityShow("cctv") }
        when {facility.facilitiesWifi == 1 -> contentDormView.getFacilityShow("wifi") }
        when {facility.facilitiesPark == 1 -> contentDormView.getFacilityShow("park") }
    }

    fun hasFacilityPosition(roomCategory: ItemRoomCategoryModel, position: Int) {
        Log.v("hasFacilityPosition" , "hasFacilityPosition : ${roomCategory.items[position].facilities} : position : $position")
        when (position){
            0 -> {
                when{roomCategory.items[position].facilities.facilitiesBed == 1 -> contentDormView.getFacilityShowNormal("bed")}
                when{roomCategory.items[position].facilities.facilitiesAirCon == 1 -> contentDormView.getFacilityShowNormal("aircon")}
                when{roomCategory.items[position].facilities.facilitiesWifi == 1 -> contentDormView.getFacilityShowNormal("wifi")}
                when{roomCategory.items[position].facilities.facilitiesCableTv == 1 -> contentDormView.getFacilityShowNormal("cabletv")}
                when{roomCategory.items[position].facilities.facilitiesTable == 1 -> contentDormView.getFacilityShowNormal("table")}
                when{roomCategory.items[position].facilities.facilitiesFurniture == 1 -> contentDormView.getFacilityShowNormal("furniture")}
                when{roomCategory.items[position].facilities.facilitiesWardrobe == 1 -> contentDormView.getFacilityShowNormal("wardrobe")}
            }
            1 -> {
                when{roomCategory.items[position].facilities.facilitiesBed == 1 -> contentDormView.getFacilitySuite("bed")}
                when{roomCategory.items[position].facilities.facilitiesAirCon == 1 -> contentDormView.getFacilitySuite("aircon")}
                when{roomCategory.items[position].facilities.facilitiesWifi == 1 -> contentDormView.getFacilitySuite("wifi")}
                when{roomCategory.items[position].facilities.facilitiesCableTv == 1 -> contentDormView.getFacilitySuite("cabletv")}
                when{roomCategory.items[position].facilities.facilitiesTable == 1 -> contentDormView.getFacilitySuite("table")}
                when{roomCategory.items[position].facilities.facilitiesFurniture == 1 -> contentDormView.getFacilitySuite("furniture")}
                when{roomCategory.items[position].facilities.facilitiesWardrobe == 1 -> contentDormView.getFacilitySuite("wardrobe")}
            }
        }

    }
}