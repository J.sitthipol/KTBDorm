package com.example.riderodead.ktbdorm.Model.SomeDorm

import com.google.gson.annotations.SerializedName

data class ReservatedCategoryModel (
        @SerializedName("items")
        val items : ArrayList<ReservationDateModel>?
)