package com.example.riderodead.ktbdorm.f05_Home

import android.content.Context
import android.os.Bundle
import android.support.v4.content.res.ResourcesCompat
import android.support.v7.app.AppCompatActivity
import com.example.riderodead.ktbdorm.R
import com.example.riderodead.ktbdorm.adapter.SlidingImageAdapter
import com.example.riderodead.ktbdorm.f05_Home.sf02MainContent.ContentDayFragment
import com.example.riderodead.ktbdorm.f05_Home.sf02MainContent.ContentMonthFragment
import kotlinx.android.synthetic.main.activity_dorm_main.*

class DormMainActivity : AppCompatActivity() {

    private var checkRentType = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dorm_main)
        initInstances()
    }

    private fun initInstances() {
        onClick()
        back_login.bringToFront()
        setSlideImageView()
        attachMainPage()
        setTypeRent("daily")
    }

    private fun setSlideImageView() {
        val slidingImageAdapter = SlidingImageAdapter(this)
        view_pager_imageSlide.adapter = slidingImageAdapter
        indictor_image_slide.attachToPager(view_pager_imageSlide)
    }

    private fun setTypeRent(typeRent: String) {
        val sp = getSharedPreferences("PREF_RENT_TYPE", Context.MODE_PRIVATE)
        val editor = sp.edit()
        editor.putString("Rent_Type", typeRent)
        editor.apply()
    }

    private fun onClick() {

        btn_day.setOnClickListener {
            if (checkRentType) {
                supportFragmentManager.beginTransaction()
                        .setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right)
                        .replace(R.id.content_maindorm_container, ContentDayFragment())
                        .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left)
                        .commit()
                setTypeRent("daily")
                selectedToolbar("day")
                checkRentType = false
            }
        }
        btn_month.setOnClickListener {
            if (!checkRentType) {
                supportFragmentManager.beginTransaction()
                        .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left)
                        .replace(R.id.content_maindorm_container, ContentMonthFragment())
                        .commit()
                setTypeRent("month")
                selectedToolbar("month")
                checkRentType = true
            }
        }

        back_login.setOnClickListener {
            onBackPressed()
        }
    }

    private fun selectedToolbar(ref : String){
        if (ref == "day"){
            val dw = ResourcesCompat.getDrawable(resources,R.drawable.selector_tritangle,null)
            btn_day.setCompoundDrawablesWithIntrinsicBounds(null,null,null,dw)
            btn_month.setCompoundDrawablesWithIntrinsicBounds(null,null,null,null)
            constraint_toolbar.setBackgroundResource(R.drawable.background_toolbar_day)
        }else{
            val dw = ResourcesCompat.getDrawable(resources,R.drawable.selector_tritangle,null)
            btn_month.setCompoundDrawablesWithIntrinsicBounds(null,null,null,dw)
            btn_day.setCompoundDrawablesWithIntrinsicBounds(null,null,null,null)
            constraint_toolbar.setBackgroundResource(R.drawable.background_toolbar_month)
        }
    }

    private fun attachMainPage() {
        supportFragmentManager.beginTransaction()
                .replace(R.id.content_maindorm_container, ContentDayFragment())
                .commit()
        selectedToolbar("day")
    }
}
