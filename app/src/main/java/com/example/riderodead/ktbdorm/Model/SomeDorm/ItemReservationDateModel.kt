package com.example.riderodead.ktbdorm.Model.SomeDorm

import com.google.gson.annotations.SerializedName

data class ItemReservationDateModel(
        @SerializedName("items")
        val items : ArrayList<ReservationDateModel>
)