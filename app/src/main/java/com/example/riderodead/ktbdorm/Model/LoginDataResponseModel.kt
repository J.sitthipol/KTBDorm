package com.example.riderodead.ktbdorm.Model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class LoginDataResponseModel(
        @SerializedName("message")
        val message: String,
        @SerializedName("unique_id")
        @Expose
        val uniqueId: String,
        @SerializedName("email")
        @Expose
        val email: String,
        @SerializedName("full_name")
        @Expose
        val fullName: String,
        @SerializedName("address")
        @Expose
        val address: String,
        @SerializedName("phone")
        @Expose
        val phone: String,
        @SerializedName("id_card")
        @Expose
        val idCard: String)



