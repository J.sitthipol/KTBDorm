package com.example.riderodead.ktbdorm.f02_Login

import com.example.riderodead.ktbdorm.DormService.LoginService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class LoginPresenter(private val loginContract: LoginContract) {
    private val client by lazy {
        LoginService.crate()
    }
    private var disposable: Disposable? = null

    fun setUpService(emailLogin: String, password: String) {
        disposable = client.serviceLogin(emailLogin, password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { it -> loginContract.serviceOK(it.status)},
                        { error -> error.message }
                )
    }

    fun disposable(): Unit? {
        return disposable?.dispose()
    }
    fun isEmailValid(email: CharSequence) {
        loginContract.checkEmail(android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches())
    }

    fun checkNull(emailLogin: String, password: String) {
        when {
            emailLogin.isBlank() && password.isBlank() -> loginContract.checkNull(false)
            emailLogin.isNotEmpty() && password.isNotEmpty() -> loginContract.checkNull(true)
        }
    }
}