package com.example.riderodead.ktbdorm.f03_Register

interface RegisterContract {
    fun checkPassword(passwordCheck: Boolean)
    fun checkEmail(emailCheck: Boolean)
    fun servicesOK(status: Boolean)
    fun checkAllField(allCorrect: Boolean)
}