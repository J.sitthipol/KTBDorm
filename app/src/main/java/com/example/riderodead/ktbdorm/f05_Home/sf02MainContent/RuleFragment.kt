package com.example.riderodead.ktbdorm.f05_Home.sf02MainContent

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.riderodead.ktbdorm.R
import kotlinx.android.synthetic.main.fragment_rule.view.*

class RuleFragment : Fragment(){
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_rule,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onClick(view)
    }

    private fun onClick(view: View) {
        view.imageview_close.setOnClickListener {
            activity!!.supportFragmentManager.beginTransaction()
                    .remove(this)
                    .commit()
        }
    }
}