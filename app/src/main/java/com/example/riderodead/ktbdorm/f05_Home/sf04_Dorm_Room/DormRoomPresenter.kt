package com.example.riderodead.ktbdorm.f05_Home.sf04_Dorm_Room

import android.annotation.SuppressLint
import android.content.Context
import android.widget.Toast
import java.text.SimpleDateFormat
import java.util.*

class DormRoomPresenter(private val dormRoomView: DormRoomView){

    private fun setDuration(dateStart:Calendar,dateEnd:Calendar): Long {
        return (dateEnd.timeInMillis - dateStart.timeInMillis) / (24 * 60 * 60 * 1000)
    }

    @SuppressLint("SimpleDateFormat")
    fun setShardPreferent(context: Context, yearStart : Int, monthStart : Int, dayStart : Int, yearEnd : Int, monthEnd : Int, dayEnd : Int) {
        val dateStart = Calendar.getInstance()
        val dateEnd = Calendar.getInstance()
        setCalendar(dateStart, dayStart, monthStart, yearStart)
        setCalendar(dateEnd, dayEnd, monthEnd, yearEnd)
        val duration = setDuration(dateStart,dateEnd)
        if (duration > 0) {
            var splitString = splitString(dateStart)
            val checkIn = setDateCheckInCheckOut(splitString)
            splitString = splitString(dateEnd)
            val checkOut = setDateCheckInCheckOut(splitString)
            val sp = context.getSharedPreferences("PREF_RESERVED", Context.MODE_PRIVATE)
            val editor = sp.edit()
            val dateFormat = SimpleDateFormat("yyyy/MM/dd")
            val dateStartFormatService = dateFormat.format(dateStart.time)
            val dateEndFormatService = dateFormat.format(dateEnd.time)
            editor.putLong("Duration", duration)
            editor.putString("checkIn", checkIn)
            editor.putString("checkOut", checkOut)
            editor.putString("DateStartFormat", dateStartFormatService)
            editor.putString("DateEndFormat", dateEndFormatService)
            dormRoomView.getShardPreference(true)
            editor.apply()
        }else{
            Toast.makeText(context,"ไม่สามารถเลือกวันเดียวกันได้",Toast.LENGTH_SHORT).show()
            dormRoomView.getShardPreference(false)
        }
    }

    private fun setDateCheckInCheckOut(splitString: List<String>) = splitString[0] + ", " + splitString[2] + " " + splitString[1] + " " + splitString[5]

    private fun splitString(dateStart: Calendar) = dateStart.time.toString().split(" ")

    private fun setCalendar(dateStart:Calendar,day: Int, month: Int, year: Int) {
        dateStart.set(Calendar.DAY_OF_MONTH, day)
        dateStart.set(Calendar.MONTH, month)
        dateStart.set(Calendar.YEAR, year)
    }

}