package com.example.riderodead.ktbdorm.Model.SomeDorm

import com.google.gson.annotations.SerializedName

data class ItemStringModel(

        @SerializedName("items")
        val items: ArrayList<String?>
)