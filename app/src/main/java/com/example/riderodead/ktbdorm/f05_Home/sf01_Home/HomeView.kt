package com.example.riderodead.ktbdorm.f05_Home.sf01_Home

import com.example.riderodead.ktbdorm.Model.AllDorm.DormDataResponseModel
import com.example.riderodead.ktbdorm.Model.AllDorm.DormResponseModel

interface HomeView {
    fun getListDorm(listDormModel : DormResponseModel)
    fun searchDorm(Dorm : DormDataResponseModel)
}