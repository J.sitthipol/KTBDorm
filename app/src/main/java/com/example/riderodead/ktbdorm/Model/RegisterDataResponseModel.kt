package com.example.riderodead.ktbdorm.Model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class RegisterDataResponseModel(
        @SerializedName("message")
        @Expose
        var message: String


)