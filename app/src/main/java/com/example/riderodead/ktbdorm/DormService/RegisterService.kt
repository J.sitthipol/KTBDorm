package com.example.riderodead.ktbdorm.DormService

import com.example.riderodead.ktbdorm.Model.RegisterResponseModel
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface RegisterService {
    @FormUrlEncoded
    @POST("UserRegister.php")
    fun addService(@Field("email") email: String,
                   @Field("password") password: String,
                   @Field("full_name") fullName: String,
                   @Field("address") address: String,
                   @Field("id_card") idCard: String,
                   @Field("phone") phone: String,
                   @Field("birth_day") birthDay: String): Observable<RegisterResponseModel>

    companion object {
        fun crate(): RegisterService {

            val retrofit = Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl("http://103.253.75.169/~cmedorm/backend/include/api/user/")
                    .build()
            return retrofit.create(RegisterService::class.java)

        }
    }

}