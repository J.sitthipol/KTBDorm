package com.example.riderodead.ktbdorm.Model.SomeDorm

import com.google.gson.annotations.SerializedName

data class RoomCategoryGalleryModel(
        @SerializedName("room_category_gallery")
        val items: ItemStringModel
)