package com.example.riderodead.ktbdorm.Model.AllDorm

import com.google.gson.annotations.SerializedName

data class ItemIntResponseModel(
        @SerializedName("items")
        val items: ArrayList<Int>
)