package com.example.riderodead.ktbdorm.f07_Payment

interface PaymentContract {
    fun checkAlready3(already3: Boolean)
    fun calculate(total: String)
}