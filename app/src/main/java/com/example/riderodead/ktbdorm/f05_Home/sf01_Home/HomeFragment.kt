package com.example.riderodead.ktbdorm.f05_Home.sf01_Home

import com.example.riderodead.ktbdorm.Model.AllDorm.DormDataResponseModel
import com.example.riderodead.ktbdorm.Model.AllDorm.DormResponseModel
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.riderodead.ktbdorm.R
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment : Fragment(), HomeView {

    private var mContext: Context? = null
    private var presenter = HomePresenter(this)

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        mContext = context
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initInstances(view)
    }

    private fun initInstances(view: View) {
        val sp = activity!!.getSharedPreferences("PREF_SEARCH",Context.MODE_PRIVATE)
        img_search.setOnClickListener {
            val editor = sp.edit()
            val search = edittext_searchbar.text.toString()
            editor.putString("Search_Name",search)
            editor.apply()
        }

        presenter.getAllDorm(view)
    }

    override fun searchDorm(Dorm: DormDataResponseModel) {
        when {
            true -> {

            }
        }
    }


    override fun getListDorm(listDormModel: DormResponseModel) {
    }

}