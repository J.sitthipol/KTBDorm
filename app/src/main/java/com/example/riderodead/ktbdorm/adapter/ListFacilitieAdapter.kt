package com.example.riderodead.ktbdorm.adapter

import com.example.riderodead.ktbdorm.HolderView.FacilitieViewHolder
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.riderodead.ktbdorm.R


class ListFacilitieAdapter(private val context: Context,private val listFacility:ArrayList<String>) : RecyclerView.Adapter<FacilitieViewHolder>(){

    private var count = -1
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FacilitieViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.layout_service,parent,false)
        Log.e("list", viewType.toString())
        count++
        val facility = FacilitieViewHolder(view)
        facility.setView(view,listFacility[count])
        return facility
    }

    override fun getItemCount(): Int = listFacility.size

    override fun onBindViewHolder(holder: FacilitieViewHolder, position: Int) {
        holder.viewHolder(context,position)
    }

}