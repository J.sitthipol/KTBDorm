package com.example.riderodead.ktbdorm.Model.SomeDorm

import com.google.gson.annotations.SerializedName

data class InfoCategoryModel(
        @SerializedName("title")
        val title: String,
        @SerializedName("size")
        val size: Int,
        @SerializedName("toilet")
        val toilet: Int,
        @SerializedName("bedroom")
        val bedroom: Int,
        @SerializedName("hall")
        val hall: Int
)