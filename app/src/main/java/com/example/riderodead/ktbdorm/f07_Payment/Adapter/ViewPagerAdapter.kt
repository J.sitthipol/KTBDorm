package com.example.riderodead.ktbdorm.f07_Payment.Adapter

import android.content.Intent
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.riderodead.ktbdorm.Model.CreditCardModel
import com.example.riderodead.ktbdorm.R
import com.example.riderodead.ktbdorm.f07_Payment.sf07_AddCardActivity.AddCardActivity
import kotlinx.android.synthetic.main.layout_credit_card.view.*

class ViewPagerAdapter(private var creditCardList: ArrayList<CreditCardModel>, private var already3: Boolean) : PagerAdapter() {

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun getItemPosition(`object`: Any): Int {
        return super.getItemPosition(`object`)
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val layout = LayoutInflater.from(container.context)
                .inflate(R.layout.layout_credit_card, container, false) as ViewGroup
        container.addView(layout)
        setDataOnPager(layout,position,creditCardList)
        onClick(layout,position,creditCardList)
        return layout
    }

    private fun onClick(layout: ViewGroup, position: Int, creditCardList: ArrayList<CreditCardModel>) {
        layout.cardview_caditcard.setOnClickListener {
            when (position) {
                creditCardList.size - 1 -> {
                    val intent = Intent(layout.context,AddCardActivity::class.java)
                    layout.context.startActivity(intent)
                }
            }
        }
    }

    private fun setDataOnPager(layout: ViewGroup, position: Int, creditCardList: ArrayList<CreditCardModel>) {
        layout.textView_account_id.text = creditCardList[position].accountId
        layout.textView_expired_date.text = creditCardList[position].expiredDate
        when {
            already3 -> {
                setUIVisible(true,layout)
            }
            else -> {
                when (position) {
                    creditCardList.size - 1 -> {
                        setUIVisible(false, layout)

                    }
                    else -> {
                        setUIVisible(true, layout)
                    }
                }
            }
        }
    }

    private fun setUIVisible(showCard: Boolean, layout: ViewGroup) {
        when (showCard){
            true -> {
                layout.textView_account_id.visibility = View.VISIBLE
                layout.textView_expired_date.visibility = View.VISIBLE
                layout.textView_add_new_credit_card.visibility = View.GONE
                layout.img_add_card.visibility = View.GONE
            }
            false -> {
                layout.textView_account_id.visibility = View.GONE
                layout.textView_expired_date.visibility = View.GONE
                layout.textView_add_new_credit_card.visibility = View.VISIBLE
                layout.img_add_card.visibility = View.VISIBLE
            }
        }
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    override fun getCount(): Int {
        return creditCardList.size
    }
}

