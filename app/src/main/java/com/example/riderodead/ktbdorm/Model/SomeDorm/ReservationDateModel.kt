package com.example.riderodead.ktbdorm.Model.SomeDorm

import com.google.gson.annotations.SerializedName

data class ReservationDateModel(
        @SerializedName("date_start")
        val dateStart : String,
        @SerializedName("date_end")
        val dateEnd : String
)