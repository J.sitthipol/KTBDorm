package com.example.riderodead.ktbdorm.Model.SomeDorm

import com.google.gson.annotations.SerializedName

data class SomeDormModel(
        @SerializedName("status")
        val status : Boolean,
        @SerializedName("data")
        val data : SomeDormDataModel
)