package com.example.riderodead.ktbdorm.adapter

import android.content.Context
import android.support.v4.view.PagerAdapter
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.example.riderodead.ktbdorm.R
import kotlinx.android.synthetic.main.layout_slidingimages.view.*

class DormRoomSlidingImageAdapter(private var context: Context?) : PagerAdapter() {

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val layout = LayoutInflater.from(container.context)
                .inflate(R.layout.layout_slidingimages,container,false) as ViewGroup
        setImage(layout)
        container.addView(layout)
        return layout
    }

    private fun setImage(layout: ViewGroup) {
        val sp = context?.getSharedPreferences("PREF_ROOM_CATEGORY",Context.MODE_PRIVATE)
        val dormId = sp?.getInt("Id",2)
        val roomCategoryId = sp?.getInt("RoomCategory", 3)
        val pictureName = sp?.getString("picName", "1.png")
        Log.v("Benz","$dormId/$roomCategoryId/$pictureName")
        Glide.with(context!!)
                .load("http://103.253.75.169/~cmedorm/backend/assets/dorm_room_category_gallery/$dormId/$roomCategoryId/$pictureName")
                .apply(RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(layout.image_review)
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View?)
    }

    override fun getItemPosition(`object`: Any): Int {
        return PagerAdapter.POSITION_NONE
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return position.toString()
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun getCount(): Int {
        return 1
    }

}
