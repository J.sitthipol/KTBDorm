package com.example.riderodead.ktbdorm.Model.SomeDorm

import com.google.gson.annotations.SerializedName

data class FloorModel(
        @SerializedName("floor_start")
        val floorStart: Int,
        @SerializedName("floor_end")
        val floorEnd: Int
)