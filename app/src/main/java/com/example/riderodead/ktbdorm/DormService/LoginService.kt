package com.example.riderodead.ktbdorm.DormService

import com.example.riderodead.ktbdorm.Model.LoginResponseModel
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface LoginService{
    @FormUrlEncoded
    @POST("UserLogin.php")
    fun  serviceLogin(@Field ("email")email: String,
                      @Field ("password")password: String): Observable<LoginResponseModel>

    companion object {
        fun crate(): LoginService{

            val retrofit = Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl("http://103.253.75.169/~cmedorm/backend/include/api/user/")
                    .build()
            return retrofit.create(LoginService::class.java)

        }
    }

}