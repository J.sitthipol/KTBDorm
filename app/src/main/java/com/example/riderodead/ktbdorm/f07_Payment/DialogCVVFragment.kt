package com.example.riderodead.ktbdorm.f07_Payment

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.riderodead.ktbdorm.R
import kotlinx.android.synthetic.main.dialog_ccv.*

class DialogCVVFragment : DialogFragment(){
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dialog_ccv,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onClick()
    }

    private fun onClick() {
        button_toast_cvv.setOnClickListener {
            dialog.dismiss()
        }
    }
}