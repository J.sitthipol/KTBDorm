package com.example.riderodead.ktbdorm.Model.SomeDorm

import com.google.gson.annotations.SerializedName

data class AddressModel (
        @SerializedName("address")
        val address: String,
        @SerializedName("address_near")
        val addressNear: String,
        @SerializedName("address_lat")
        val addressLat: Double,
        @SerializedName("address_lng")
        val addressLng: Double
)