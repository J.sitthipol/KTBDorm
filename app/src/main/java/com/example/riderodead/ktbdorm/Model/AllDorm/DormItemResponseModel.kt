package com.example.riderodead.ktbdorm.Model.AllDorm

import com.google.gson.annotations.SerializedName

data class DormItemResponseModel(
    @SerializedName("items")
    val items : ArrayList<DormDataResponseModel>
)