package com.example.riderodead.ktbdorm.Model.SomeDorm

import com.google.gson.annotations.SerializedName

data class InfoModel(
        @SerializedName("title")
        val title: String,
        @SerializedName("description")
        val description: String,
        @SerializedName("all_floors")
        val allFloors: Int,
        @SerializedName("all_rooms")
        val allRooms: Int
)