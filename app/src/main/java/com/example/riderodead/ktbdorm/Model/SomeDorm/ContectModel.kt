package com.example.riderodead.ktbdorm.Model.SomeDorm

import com.google.gson.annotations.SerializedName

data class ContectModel(
        @SerializedName("contact_name")
        val contactName: String,
        @SerializedName("contact_phone")
        val contactPhone: String,
        @SerializedName("contact_email")
        val contactEmail: String
)