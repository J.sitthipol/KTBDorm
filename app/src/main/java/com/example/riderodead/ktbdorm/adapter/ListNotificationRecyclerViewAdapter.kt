package com.example.riderodead.ktbdorm.adapter

import com.example.riderodead.ktbdorm.HolderView.NotificationViewHolder
import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.riderodead.ktbdorm.R
import com.example.riderodead.ktbdorm.f05_Home.sf06_BeforePayment.BeforePaymentActivity

class ListNotificationRecyclerViewAdapter(private val context:Context) : RecyclerView.Adapter<NotificationViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotificationViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.layout_notification,parent,false)
        return NotificationViewHolder(view)
    }

    override fun getItemCount(): Int = 3

    override fun onBindViewHolder(holder: NotificationViewHolder, position: Int) {
        onClick(holder)
    }

    private fun onClick(holder: NotificationViewHolder) {
        holder.buttonPayment.setOnClickListener {
            val intent = Intent(context,BeforePaymentActivity::class.java)
            context.startActivity(intent)
        }
    }

}