package com.example.riderodead.ktbdorm.f07_Payment


import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import com.example.riderodead.ktbdorm.Model.CreditCardModel
import com.example.riderodead.ktbdorm.R
import com.example.riderodead.ktbdorm.f07_Payment.Adapter.ViewPagerAdapter
import kotlinx.android.synthetic.main.fragment_payment_choose_card.*

class PaymentChooseCardFragment : Fragment(), PaymentContract {

    private var creditCardList = ArrayList<CreditCardModel>()
    private var isAlready3: Boolean = false
    private var paymentPresenter: PaymentPresenter? = null
    private var adapter : ViewPagerAdapter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_payment_choose_card, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        paymentPresenter = PaymentPresenter(this)
        initInstances(view)
    }

    private fun initInstances(view: View) {
        view.setOnTouchListener { keyboardView, _ ->
            Log.v("PaymentChooseCard","layout on click")
            hideKeyboard(keyboardView) }
        addCreditCardList()
        setViewPager()
        onClick()
    }

    private fun onClick() {
        btn_check_information.setOnClickListener{
            sendAmountToConfirm()
            activity?.supportFragmentManager?.beginTransaction()
                    ?.replace(R.id.content_container, ChoosePaymentConfirmFragment())
                    ?.commit()
        }
    }

    private fun hideKeyboard(view: View): Boolean {
        val imm = activity?.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        return imm.hideSoftInputFromWindow(view.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
    }

    private fun setViewPager() {
        viewPager.adapter = ViewPagerAdapter(creditCardList,isAlready3)
        indicator.attachToPager(viewPager)
    }

    private fun addCreditCardList() {
        var creditCardModel2 = CreditCardModel("id 2", "date 2")
        var creditCardModel3 = CreditCardModel("id 3", "date 3")
        val emptyArrayList = CreditCardModel("", "")

        paymentPresenter?.checkModelAlready3(creditCardList)

        when (isAlready3){
            false -> creditCardList.add(emptyArrayList)
        }
    }

    private fun sendAmountToConfirm(){
        val sp = activity?.getSharedPreferences("PREF_AMOUNT", Context.MODE_PRIVATE)
        val editor = sp?.edit()
        editor?.putString("Amount", editText_amount.text.toString())
        editor?.apply()
    }

    override fun checkAlready3(already3: Boolean) {
        isAlready3 = already3
    }

    override fun calculate(total: String) {

    }
}
