package com.example.riderodead.ktbdorm.f02_Login

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.example.riderodead.ktbdorm.R
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        initInstances()
    }

    private fun initInstances() {
       attachLogin()
        back_login.setOnClickListener { onBackPressed()}
    }

    private fun attachLogin() {
        supportFragmentManager.beginTransaction()
                .replace(R.id.content_container, LoginFragment())
                .commit()
    }

}
