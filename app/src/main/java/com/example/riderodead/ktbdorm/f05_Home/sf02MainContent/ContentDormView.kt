package com.example.riderodead.ktbdorm.f05_Home.sf02MainContent

import com.example.riderodead.ktbdorm.Model.SomeDorm.SomeDormModel

interface ContentDormView {
        fun getInformationDorm(items : SomeDormModel)
        fun getFacilityShow(showFacility: String)
        fun getFacilityShowNormal(showFacilityNormal: String)
        fun getFacilitySuite(showFacilitySuite: String)
}