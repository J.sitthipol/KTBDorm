package com.example.riderodead.ktbdorm.f05_Home

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.riderodead.ktbdorm.R
import com.example.riderodead.ktbdorm.adapter.SlidingImageFullScreenAdapter
import kotlinx.android.synthetic.main.fragment_photo_full.*
import kotlinx.android.synthetic.main.fragment_photo_full.view.*

class FragmentFullScreen : Fragment(){
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_photo_full,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initInstances(view)
    }

    private fun initInstances(view: View) {
        val drawableArray = ArrayList<Int>()
        drawableArray.add(R.drawable.img_dorm)
        drawableArray.add(R.drawable.img_dorm2)
        ib_close.setOnClickListener {
            activity!!.supportFragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right)
                .remove(this)
                .commit() }
        view.pagerview_fullscreen.adapter = SlidingImageFullScreenAdapter(drawableArray)

    }
}
