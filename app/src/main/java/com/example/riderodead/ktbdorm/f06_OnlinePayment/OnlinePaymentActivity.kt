package com.example.riderodead.ktbdorm.f06_OnlinePayment

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.riderodead.ktbdorm.R
import kotlinx.android.synthetic.main.activity_online_payment.*

class OnlinePaymentActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_online_payment)
        initInstances()
    }

    private fun initInstances() {
        back.setOnClickListener { onBackPressed() }
    }
}
