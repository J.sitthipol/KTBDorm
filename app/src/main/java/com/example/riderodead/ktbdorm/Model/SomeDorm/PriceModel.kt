package com.example.riderodead.ktbdorm.Model.SomeDorm

import com.google.gson.annotations.SerializedName

data class PriceModel(
        @SerializedName("price_daily")
        val priceDaily: Int,
        @SerializedName("price_monthly")
        val priceMonthly: Int
)