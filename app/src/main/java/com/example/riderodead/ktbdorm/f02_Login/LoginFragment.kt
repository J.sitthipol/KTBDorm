package com.example.riderodead.ktbdorm.f02_Login


import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.PorterDuff
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.*
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import com.example.riderodead.ktbdorm.R
import com.example.riderodead.ktbdorm.f04_ForgotPassword.ForgotPasswordActivity
import com.example.riderodead.ktbdorm.f05_Home.sf01_Home.HomeActivity
import kotlinx.android.synthetic.main.fragment_login.*


class LoginFragment : Fragment(), LoginContract {

    private var mContext: Context? = null

    private var loginPresenter: LoginPresenter? = null

    private lateinit var emailLogin: String

    private lateinit var password: String

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loginPresenter = LoginPresenter(this)
        initInstances(view)
    }

    private fun initInstances(rootView: View) {
        viewOnClick(rootView)
        getTextFromEditText()
    }

    override fun onPause() {
        super.onPause()
        loginPresenter?.disposable()
    }

    private fun viewOnClick(rootView: View) {
        button_sign_in.setOnClickListener {
            getTextFromEditText()
            loginPresenter?.checkNull(emailLogin, password)
        }
        textview_forgot_password.setOnClickListener { intentActivity("forgot") }
        rootView.setOnTouchListener { v, _ -> hideKeyboard(v) }
        edittext_password.setOnEditorActionListener { _, actionId, event ->
            if ((event != null && (event.keyCode == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                getTextFromEditText()
                loginPresenter?.checkNull(emailLogin, password)
            }
            false
        }

    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        mContext = context
    }

    private fun intentActivity(intent: String) {
        when (intent) {
            "forgot" -> {
                val forgotPasswordIntent = Intent(activity, ForgotPasswordActivity::class.java)
                startActivity(forgotPasswordIntent)
            }
            "home" -> {
                val homeIntent = Intent(activity, HomeActivity::class.java)
                startActivity(homeIntent)
            }
        }
    }

    private fun getTextFromEditText() {
        emailLogin = edittext_email_login.text.toString()
        password = edittext_password.text.toString()
    }

    private fun hideKeyboard(view: View): Boolean {
        val imm = activity?.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        return imm.hideSoftInputFromWindow(view.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
    }

    override fun serviceOK(status: Boolean) {
        when {
            status -> {
                edittext_email_login.background.setColorFilter(ContextCompat.getColor(mContext!!, R.color.colorLineGray), PorterDuff.Mode.SRC_IN)
                email_and_password_fail.visibility = View.GONE
                intentActivity("home")
            }
            else -> {
                showError("emailAndPassword")
            }
        }
    }

    override fun checkEmail(emailChecked: Boolean) {
        when {
            !emailChecked -> {
                showError("email")
            }
        }
    }

    override fun checkNull(nullChecked: Boolean) {
        when {
            nullChecked -> {
                loginPresenter?.isEmailValid(emailLogin)
                loginPresenter?.setUpService(emailLogin, password)
            }
        }
    }

    private fun showError(error: String) {
       when (error){
           "email" -> {
               edittext_email_login.background.setColorFilter(ContextCompat.getColor(mContext!!, R.color.colorRed), PorterDuff.Mode.SRC_IN)
               email_and_password_fail.text = getString(R.string.email_password_not_correct)
               email_and_password_fail.visibility = View.VISIBLE
           }
           "emailAndPassword" -> {
               edittext_email_login.background.setColorFilter(ContextCompat.getColor(mContext!!, R.color.colorRed), PorterDuff.Mode.SRC_IN)
               edittext_password.background.setColorFilter(ContextCompat.getColor(mContext!!, R.color.colorRed), PorterDuff.Mode.SRC_IN)
               email_and_password_fail.text = getString(R.string.email_password_not_correct)
               email_and_password_fail.visibility = View.VISIBLE
           }

       }
    }
}
