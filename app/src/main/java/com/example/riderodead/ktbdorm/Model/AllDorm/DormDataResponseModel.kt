package com.example.riderodead.ktbdorm.Model.AllDorm

import com.google.gson.annotations.SerializedName

data class DormDataResponseModel(
    @SerializedName("dorm_id")
    val dormId: Int,

    @SerializedName("title")
    val title: String,

    @SerializedName("address")
    val address: String,


    @SerializedName("contact_name")
    val contactName: String,

    @SerializedName("contact_phone")
    val contactPhone: String,

    @SerializedName("remaining_room")
    val remainingRoom: Int,

    @SerializedName("all_room")
    val allRoom: Int,

    @SerializedName("room_category")
    val roomCategory: PriceResponseModel
)