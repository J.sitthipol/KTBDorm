package com.example.riderodead.ktbdorm.f08_QRCodePayment

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.example.riderodead.ktbdorm.R
import com.example.riderodead.ktbdorm.f07_Payment.PaymentActivity
import com.google.zxing.Result
import kotlinx.android.synthetic.main.activity_qrcode.*
import me.dm7.barcodescanner.zxing.ZXingScannerView

class QRCodeActivity : AppCompatActivity(),QRCodeView,ZXingScannerView.ResultHandler {
    override fun checkApi(isCurrentApi : Boolean) {
        if (isCurrentApi){
            if (scannerView == null)
                scannerView = ZxingScannerView
            scannerView.setResultHandler(this)
            scannerView.startCamera()
        }
    }

    private val presenter = QRCodePresenter(this)

    override fun handleResult(p0: Result?) {
        Toast.makeText(this,p0!!.text,Toast.LENGTH_SHORT).show()
        val intent = Intent(this,PaymentActivity::class.java)
        startActivity(intent)
    }

    private lateinit var scannerView : ZXingScannerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_qrcode)
        initInstances()
    }

    private fun initInstances() {
        back.setOnClickListener { onBackPressed() }
        presenter.setContext(this)
        showScanQR()

    }

    private fun showScanQR() {
        scannerView = ZxingScannerView
    }

    override fun onResume() {
        super.onResume()
        presenter.checkCameraApiVersion()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        scannerView.stopCamera()
    }
    override fun onDestroy() {
        super.onDestroy()
        scannerView.stopCamera()
    }
}
