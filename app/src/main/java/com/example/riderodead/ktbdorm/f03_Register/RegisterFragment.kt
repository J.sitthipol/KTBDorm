package com.example.riderodead.ktbdorm.f03_Register


import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.graphics.PorterDuff
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import com.example.riderodead.ktbdorm.DatePicker.MyDatePickerFragment
import com.example.riderodead.ktbdorm.R
import kotlinx.android.synthetic.main.fragment_register.*

class RegisterFragment : Fragment(), RegisterContract, MyDatePickerFragment.SendDateListener {

    private var termClicked: Boolean = true

    private var mContext: Context? = null

    private var email = ""

    private var password = ""

    private var fullName = ""

    private var address = ""

    private var idCard = ""

    private var birthDay = ""

    private var phone = ""

    private var rePassword = ""

    private var field: Boolean = false

    private lateinit var registerPresenter: RegisterPresenter

    private var passwordChecked: Boolean = false

    private var emailChecked: Boolean = false

    private var phoneChecked: Boolean = false

    private var idCardChecked: Boolean = false

    private var addressChecked: Boolean = false

    private var nameChecked: Boolean = false

    private var rePasswordChecked: Boolean = false

    private var checkTermClicked: Boolean = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_register, container, false)
    }

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        registerPresenter = RegisterPresenter(this)
        initInstance(view)
    }

    @RequiresApi(Build.VERSION_CODES.N)
    private fun initInstance(view: View) {
        checkTerm.setOnClickListener { checkTermClicked(termClicked) }
        view.setOnTouchListener { keyboardView, _ -> hideKeyboard(keyboardView) }
        textWatcher()
        editText_name_surname.setOnEditorActionListener { _, actionId, event ->
            if ((event != null && (event.keyCode == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE) || (actionId == EditorInfo.IME_ACTION_NEXT)) {
                datePicker()
            }
            false
        }
        textview_date.setOnClickListener { datePicker() }
        getTextFromEditText()
        btnClick()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        mContext = context
    }


    private fun textWatcher() {

        editText_phone_number.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(phone: Editable?) {
                if (phone!!.length == 10 && !phone.contains("-")) {
                    editText_phone_number.text = registerPresenter.setPhoneFormat(phone)
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(phone: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }
        })

        editText_id_card.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(idCard: Editable?) {
                if (idCard!!.length == 13 && !idCard.contains("-")) {
                    editText_id_card.text = registerPresenter.setIdCardFormat(idCard)
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }
        })

    }

    private fun checkAllFieldCorrect() {
        val name = editText_name_surname.text?.split(" ")
        when {
            emailChecked -> {
                editText_email.background.setColorFilter(ContextCompat.getColor(mContext!!, R.color.colorLineGray), PorterDuff.Mode.SRC_IN)
                textView_error.visibility = View.GONE
            }
            !emailChecked || editText_email.text.isNullOrBlank() -> {
                showError("email")
            }
        }
        when (birthDay) {
            "วว/ดด/ปป" -> showError("birthDay")
            else -> {
                view_date.setBackgroundResource(R.color.colorRed)
                textView_error.visibility = View.GONE
            }
        }

        when {
            passwordChecked -> {
                editText_password.background.setColorFilter(ContextCompat.getColor(mContext!!, R.color.colorLineGray), PorterDuff.Mode.SRC_IN)
                textView_error.visibility = View.GONE
            }
            else -> showError("password")
        }
        phoneChecked = when {
            phone.isBlank() -> {
                showError("phone")
                false
            }
            else -> true
        }

        idCardChecked = when {
            idCard.isBlank() -> {
                showError("idCard")
                false
            }
            else -> true
        }

        when {
            editText_address.text.isNullOrEmpty() -> {
                showError("address")
                addressChecked = false
            }
            else -> {
                editText_password.background.setColorFilter(ContextCompat.getColor(mContext!!, R.color.colorLineGray), PorterDuff.Mode.SRC_IN)
                textView_error.visibility = View.GONE
                addressChecked = true
            }

        }

        when {
            name!!.size == 2 -> {
                editText_name_surname.background.setColorFilter(ContextCompat.getColor(mContext!!, R.color.colorLineGray), PorterDuff.Mode.SRC_IN)
                textView_error.visibility = View.GONE
                nameChecked = true
            }
            name.size != 2 -> {
                showError("name")
                nameChecked = false
            }

        }
        when {
            password == rePassword && password.isNotBlank() && rePassword.isNotBlank() && password.length >= 8 && rePassword.length >= 8 -> {
                editText_rePassword.background.setColorFilter(ContextCompat.getColor(mContext!!, R.color.colorLineGray), PorterDuff.Mode.SRC_IN)
                textView_error.visibility = View.GONE
                rePasswordChecked = true
            }
            password == rePassword && password.isNotBlank() && rePassword.isNotBlank() && password.length < 8 && rePassword.length < 8 -> {
                showError("password")
                rePasswordChecked = true
            }
            password != rePassword || rePassword.isBlank() -> {
                showError("rePassword")
                rePasswordChecked = false
            }
        }
        when {
            email.isBlank() && password.isBlank() && rePassword.isBlank() && fullName.isBlank() && address.isBlank() &&
                    idCard.isBlank() && phone.isBlank() && birthDay.isBlank() -> showError("allField")
        }
        registerPresenter.allCorrect(emailChecked, passwordChecked, idCardChecked, addressChecked, nameChecked, rePasswordChecked, checkTermClicked)
    }

    private fun showError(error: String) {
        when (error) {
            "email" -> {
                editText_email.background.setColorFilter(ContextCompat.getColor(mContext!!, R.color.colorRed), PorterDuff.Mode.SRC_IN)
                textView_error.visibility = View.VISIBLE
                textView_error.text = "รูปแบบอีเมลไม่ถูกต้อง"
            }
            "birthDay" -> {
                view_date.setBackgroundResource(R.color.colorRed)
                textView_error.visibility = View.VISIBLE
                textView_error.text = "กรุณาใส่วันเกิด"
            }
            "phone" -> {
                editText_phone_number.background.setColorFilter(ContextCompat.getColor(mContext!!, R.color.colorRed), PorterDuff.Mode.SRC_IN)
            }
            "idCard" -> {
                editText_id_card.background.setColorFilter(ContextCompat.getColor(mContext!!, R.color.colorRed), PorterDuff.Mode.SRC_IN)
            }
            "password" -> {
                editText_password.background.setColorFilter(ContextCompat.getColor(mContext!!, R.color.colorRed), PorterDuff.Mode.SRC_IN)
                textView_error.visibility = View.VISIBLE
                textView_error.text = "รูปแบบรหัสผ่านไม่ถูกต้อง"
            }
            "address" -> {
                editText_address.background.setColorFilter(ContextCompat.getColor(mContext!!, R.color.colorRed), PorterDuff.Mode.SRC_IN)
                textView_error.visibility = View.VISIBLE
                textView_error.text = "กรุณากรอกที่อยู่"
            }
            "name" -> {
                editText_name_surname.background.setColorFilter(ContextCompat.getColor(mContext!!, R.color.colorRed), PorterDuff.Mode.SRC_IN)
                textView_error.text = "กรุณาใส่นามสกุล"
                textView_error.visibility = View.VISIBLE
            }
            "rePassword" -> {
                editText_rePassword.background.setColorFilter(ContextCompat.getColor(mContext!!, R.color.colorRed), PorterDuff.Mode.SRC_IN)
                textView_error.visibility = View.VISIBLE
                textView_error.text = "รหัสผ่านไม่ตรงกัน"
            }
            "allField" -> {
                textView_error.visibility = View.VISIBLE
                textView_error.text = "กรุณากรอกข้อมูลให้ครบ"
            }
        }
    }

    override fun onPause() {
        super.onPause()
        registerPresenter.disposable()
    }

    private fun goToRegisterComplete() {
        activity?.supportFragmentManager?.beginTransaction()
                ?.replace(R.id.register_content_container, RegisterCompleteFragment())
                ?.commit()
    }

    private fun btnClick() {
        button_register.setOnClickListener {
            getTextFromEditText()
            registerPresenter.isEmailValid(email)
            registerPresenter.checkPassword(password)
            checkAllFieldCorrect()
        }
    }

    private fun checkTermClicked(clicked: Boolean) {
        if (clicked) {
            checkTerm.setImageResource(R.drawable.yes_5)
            checkTermClicked = true
            termClicked = false
        } else {
            checkTerm.setImageResource(R.drawable.oval_2)
            checkTermClicked = false
            termClicked = true
        }
    }

    private fun getTextFromEditText() {
        email = editText_email.text.toString()
        password = editText_password.text.toString()
        rePassword = editText_rePassword.text.toString()
        fullName = editText_name_surname.text.toString()
        address = editText_address.text.toString()
        idCard = editText_id_card.text.toString()
        phone = editText_phone_number.text.toString()
        birthDay = textview_date.text.toString()
    }

    private fun registerDorm(field: Boolean) {
        when {
            field -> {
                registerPresenter.setUpServiceRegister(email,
                        password,
                        fullName,
                        address,
                        idCard,
                        phone,
                        birthDay)
            }
            else -> showError("allField")
        }
    }

    @RequiresApi(Build.VERSION_CODES.N)
    private fun datePicker() {
        val dialogFragment = MyDatePickerFragment()
        dialogFragment.setTargetFragment(this, 0)
        dialogFragment.show(activity?.supportFragmentManager, "com/example/riderodead/ktbdorm/DatePicker")
        dialogFragment.setOnSendDate(this)
    }

    private fun hideKeyboard(view: View): Boolean {
        val imm = activity?.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        return imm.hideSoftInputFromWindow(view.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
    }

    override fun checkPassword(passwordCheck: Boolean) {
        passwordChecked = passwordCheck
    }

    override fun checkEmail(emailCheck: Boolean) {
        emailChecked = emailCheck
    }


    override fun servicesOK(status: Boolean) {
        when {
            status -> goToRegisterComplete()
        }
    }

    override fun checkAllField(allCorrect: Boolean) {
        field = allCorrect
        registerDorm(field)
    }

    @SuppressLint("SetTextI18n")
    override fun onSendData(day: Int, month: Int, year: Int) {
        textview_date.text = "$day/$month/$year"
    }


}
