package com.example.riderodead.ktbdorm.f04_ForgotPassword


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.example.riderodead.ktbdorm.R
import kotlinx.android.synthetic.main.fragment_forgot_password_complete.*

class ForgotPasswordCompleteFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_forgot_password_complete, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initInstances()
    }

    private fun initInstances() {
        button_back_to_login.setOnClickListener { activity?.onBackPressed() }
    }
}
