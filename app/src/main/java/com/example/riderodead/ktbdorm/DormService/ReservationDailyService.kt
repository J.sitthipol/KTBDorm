package com.example.riderodead.ktbdorm.DormService

import com.example.riderodead.ktbdorm.Model.ReservationDailyResponseModel
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface ReservationDailyService {
    @FormUrlEncoded
    @POST("UserReservation.php")
    fun serviceReservation(@Field("unique_id") uniqueId: String,
                     @Field("room_category_id") roomCategoryId: Int,
                     @Field("reservation_category") reservationCategory: String,
                     @Field("date_start")dateStart: String,
                     @Field("date_end")dateEnd: String): Observable<ReservationDailyResponseModel>

    companion object {
        fun crate(): ReservationDailyService {

            val retrofit = Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl("http://103.253.75.169/~cmedorm/backend/include/api/user/")
                    .build()
            return retrofit.create(ReservationDailyService::class.java)

        }
    }

}