package com.example.riderodead.ktbdorm.Model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class LoginResponseModel(
        @Expose
        @SerializedName("status")
        val status: Boolean,
        @Expose
        @SerializedName("data")
        val data: LoginDataResponseModel
)