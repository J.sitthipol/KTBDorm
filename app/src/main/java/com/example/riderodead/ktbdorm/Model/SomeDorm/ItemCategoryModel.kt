package com.example.riderodead.ktbdorm.Model.SomeDorm

import com.google.gson.annotations.SerializedName

data class ItemCategoryModel(
        @SerializedName("room_category_id")
        val roomCategoryId: Int,
        @SerializedName("info")
        val info: InfoCategoryModel,
        @SerializedName("facilities")
        val facilities: FacilitieCategoryModel,
        @SerializedName("price")
        val price: PriceModel,
        @SerializedName("floor")
        val floor: FloorModel,
        @SerializedName("reservation")
        val reservation: ItemReservationDateModel,
        @SerializedName("room_category_gallery")
        val room_category_gallery: ItemStringModel?
)