package com.example.riderodead.ktbdorm.f02_Login


import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.riderodead.ktbdorm.R
import com.example.riderodead.ktbdorm.f03_Register.RegisterActivity
import kotlinx.android.synthetic.main.fragment_before_login.*

class BeforeLoginFragment : Fragment() {

    private var mContext: Context? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_before_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initInstances()
    }

    private fun initInstances() {
        button_sign_in.setOnClickListener {goToLogin()}
        button_create_account.setOnClickListener{ goToCreateAccount() }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        mContext = context
    }

    private fun goToLogin() {
        val loginIntent = Intent(activity,LoginActivity::class.java)
        startActivity(loginIntent)
    }
    private fun goToCreateAccount() {
        val registerIntent = Intent(activity,RegisterActivity::class.java)
        startActivity(registerIntent)

    }

}
