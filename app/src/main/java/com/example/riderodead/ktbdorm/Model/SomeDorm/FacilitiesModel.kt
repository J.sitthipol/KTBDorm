package com.example.riderodead.ktbdorm.Model.SomeDorm

import com.google.gson.annotations.SerializedName

data class FacilitiesModel(
        @SerializedName("facilities_fitness")
        val facilitiesFitness: Int,
        @SerializedName("facilities_elevator")
        val facilitiesElevator: Int,
        @SerializedName("facilities_store")
        val facilitiesStore: Int,
        @SerializedName("facilities_cctv")
        val facilitiesCctv: Int,
        @SerializedName("facilities_wifi")
        val facilitiesWifi: Int,
        @SerializedName("facilities_park")
        val facilitiesPark: Int
)