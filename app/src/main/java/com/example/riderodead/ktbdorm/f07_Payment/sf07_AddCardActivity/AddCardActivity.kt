package com.example.riderodead.ktbdorm.f07_Payment.sf07_AddCardActivity

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.widget.Toast
import com.example.riderodead.ktbdorm.R
import com.example.riderodead.ktbdorm.f07_Payment.DialogCVVFragment
import com.example.riderodead.ktbdorm.f07_Payment.PaymentActivity
import kotlinx.android.synthetic.main.activity_add_card.*

class AddCardActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_card)
        initInstances()
    }

    private fun initInstances() {
        edittext_card.addTextChangedListener(OnTextChanged("number"))
        edittext_datecard.addTextChangedListener(OnTextChanged("date"))
        onClick()
    }

    private fun onClick() {
        button_addcard.setOnClickListener {
            if (edittext_card.text.length == 19
                    && edittext_datecard.text.length == 5
                    && edittext_cvv.text.length == 3
                    && edittext_membercard.text.length > 0) {

                val intent = Intent(this, PaymentActivity::class.java)
                intent.putExtra("CreditNumber",edittext_card.text.toString())
                intent.putExtra("CreditDate",edittext_datecard.text.toString())
                startActivityForResult(intent,1)
            } else {
                Toast.makeText(this, "กรุณากรอกข้อมูลให้ถูกต้องครบถ้วน", Toast.LENGTH_SHORT).show()
            }
        }

        imageview_outline.setOnClickListener {
            val dialogCVVFragment = DialogCVVFragment()
            dialogCVVFragment.show(supportFragmentManager, "CVVDialog")
        }
    }

    private fun OnTextChanged(type : String): TextWatcher? {
        when(type){
            "number" -> {
                var current = "-"
                return object : TextWatcher {
                    override fun afterTextChanged(s: Editable?) {
                        when {
                            s.toString() != current -> {
                                val userInput = s.toString().replace("[^\\d]".toRegex(), "")
                                when {
                                    userInput.length <= 16 -> {
                                        val sb = StringBuilder()
                                        for (i in 0 until userInput.length) {
                                            when {
                                                i % 4 == 0 && i > 0 -> sb.append("-")
                                            }
                                            sb.append(userInput[i])
                                        }
                                        current = sb.toString()

                                        s!!.filters = arrayOfNulls<InputFilter>(0)
                                    }
                                }
                                s!!.replace(0, s.length, current, 0, current.length)
                            }
                        }
                    }
                    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
                    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
                }
            }
            else -> {
                var current = "/"
                return object : TextWatcher {
                    override fun afterTextChanged(s: Editable?) {
                        when {
                            s.toString() != current -> {
                                val userInput = s.toString().replace("[^\\d]".toRegex(), "")
                                when {
                                    userInput.length <= 4 -> {
                                        val sb = StringBuilder()
                                        for (i in 0 until userInput.length) {
                                            when {
                                                i % 2 == 0 && i > 0 -> sb.append("/")
                                            }
                                            sb.append(userInput[i])
                                        }
                                        current = sb.toString()

                                        s!!.filters = arrayOfNulls<InputFilter>(0)
                                    }
                                }
                                s!!.replace(0, s.length, current, 0, current.length)
                            }
                        }
                    }
                    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
                    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
                }
            }
        }
    }
}
