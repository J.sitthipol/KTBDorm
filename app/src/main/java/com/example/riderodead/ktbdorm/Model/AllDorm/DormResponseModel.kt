package com.example.riderodead.ktbdorm.Model.AllDorm

import com.google.gson.annotations.SerializedName

data class DormResponseModel(
        @SerializedName("status")
        val status : Boolean,
        @SerializedName("data")
        val data : DormItemResponseModel
)