package com.example.riderodead.ktbdorm.Model.AllDorm

import com.google.gson.annotations.SerializedName

data class PriceResponseModel(
        @SerializedName("price_daily")
        val intDailyModel: ItemIntResponseModel,

        @SerializedName("price_monthly")
        val intMonthlyModel: ItemIntResponseModel
)