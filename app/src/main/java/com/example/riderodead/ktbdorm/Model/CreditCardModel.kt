package com.example.riderodead.ktbdorm.Model

import com.google.gson.annotations.SerializedName

data class CreditCardModel(
        @SerializedName("accountId")
        val accountId: String,
        @SerializedName("expiredDate")
        val expiredDate: String
)
