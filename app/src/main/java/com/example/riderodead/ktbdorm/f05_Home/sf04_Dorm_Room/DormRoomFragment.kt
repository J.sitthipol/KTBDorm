package com.example.riderodead.ktbdorm.f05_Home.sf04_Dorm_Room

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.riderodead.ktbdorm.R
import com.example.riderodead.ktbdorm.adapter.DormRoomSlidingImageAdapter
import com.example.riderodead.ktbdorm.adapter.ListFacilityRoomNormalSuiteAdapter
import com.example.riderodead.ktbdorm.f05_Home.sf05_Reserved_Room.ReservationActivity
import com.leavjenn.smoothdaterangepicker.date.SmoothDateRangePickerFragment
import kotlinx.android.synthetic.main.fragment_dorm_room.*
import java.util.*


class DormRoomFragment : Fragment(), DormRoomView {

    private var durationNotZero = false

    override fun getShardPreference(finished : Boolean) {
        when(finished){
            true -> durationNotZero = true
            false -> {}
        }

    }

    private  val presenter = DormRoomPresenter(this)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_dorm_room, container, false)
    }

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initInstance()
    }

    @RequiresApi(Build.VERSION_CODES.N)
    private fun initInstance() {
        setImageSlide()
        val sp = activity?.getSharedPreferences("PREF_ROOM_TYPE", Context.MODE_PRIVATE)
        setDataMockUp(sp)
        setButtonRent()
        val listFacilityRoomType = activity?.intent?.extras?.getStringArrayList("listFacilityRoomType")

        setUpAdapter(listFacilityRoomType)
        Log.v("Benz","DormRoom : $listFacilityRoomType")
    }

    private fun setButtonRent() {
        button_rent.setOnClickListener {
            val smoothDateRangePickerFragment = SmoothDateRangePickerFragment.newInstance {
                view, yearStart, monthStart, dayStart, yearEnd, monthEnd, dayEnd ->
                presenter.setShardPreferent(view.activity,yearStart, monthStart, dayStart, yearEnd, monthEnd, dayEnd)
                if (durationNotZero) {
                    val reservationIntent = Intent(activity, ReservationActivity::class.java)
                    startActivity(reservationIntent)
                }
            }

            smoothDateRangePickerFragment.accentColor = Color.parseColor("#3ea1e3")
            smoothDateRangePickerFragment.show(activity!!.fragmentManager,"smoothDateRangePicker")

        }
    }

    private fun setUpAdapter(listFacilityRoomType: ArrayList<String>?) {
        recycler_services.layoutManager = GridLayoutManager(context,2)
        recycler_services.adapter = ListFacilityRoomNormalSuiteAdapter(context!!, listFacilityRoomType!!)
    }

    @SuppressLint("SetTextI18n")
    private fun setDataMockUp(spRoomType: SharedPreferences?) {
        textView_room_name.text = spRoomType!!.getString("Room_Title","N/A")
        textView_floor.text = "ชั้น " + spRoomType.getInt("Room_Floor_Start", 0) + " - " + spRoomType.getInt("Room_Floor_End", 0)
        textView_price.text = spRoomType.getInt("Room_Price",0).toString() + getString(R.string.baht)
        textView_room_size_detail.text = spRoomType.getInt("Room_Size",0).toString() + " ตารางเมตร"
    }

    private fun setImageSlide() {
        val dormRoomSlidingImageAdapter = DormRoomSlidingImageAdapter( this.context)
        viewPagerImageSlide.adapter = dormRoomSlidingImageAdapter
        indictorImageSlide.attachToPager(viewPagerImageSlide)

    }
}
