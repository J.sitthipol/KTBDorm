package com.example.riderodead.ktbdorm.adapter

import android.annotation.SuppressLint
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.riderodead.ktbdorm.R
import kotlinx.android.synthetic.main.layout_photo_full.view.*

class SlidingImageFullScreenAdapter(drawableArray: ArrayList<Int>) : PagerAdapter() {

    private var pageCount : Int? = null

    private var drawable = drawableArray

    init {
        this.pageCount = drawableArray.size
    }

    @SuppressLint("SetTextI18n")
    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val layout = LayoutInflater.from(container.context)
                .inflate(R.layout.layout_photo_full, container, false) as ViewGroup
        layout.image_review.setImageResource(drawable[position])
        layout.textview_page_number.text = "${(position+1)}/$pageCount"
        container.addView(layout)
        return layout
    }


    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View?)
    }

    override fun getItemPosition(`object`: Any): Int {
        return PagerAdapter.POSITION_NONE
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return position.toString()
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun getCount(): Int = pageCount!!

}
