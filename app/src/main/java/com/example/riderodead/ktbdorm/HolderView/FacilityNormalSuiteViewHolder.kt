package com.example.riderodead.ktbdorm.HolderView

import android.annotation.SuppressLint
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.View
import com.example.riderodead.ktbdorm.R
import kotlinx.android.synthetic.main.layout_service.view.*

class FacilityNormalSuiteViewHolder(val view: View) : RecyclerView.ViewHolder(view){

    private var mContext : Context? = null

    fun viewHolder(context: Context) {
        mContext = context
    }

    @SuppressLint("SetTextI18n")
    fun setView(view:View, facility : String) : View {
        when (facility) {
            "fitness" -> {
                view.imageview_icon.setImageResource(R.drawable.ic_fitness)
                view.textview_services.text = "ฟิตเนส"
            }
            "elevator" -> {
                view.imageview_icon.setImageResource(R.drawable.ic_elevator)
                view.textview_services.text = "ลิฟท์"
            }
            "shop" -> {
                view.imageview_icon.setImageResource(R.drawable.ic_shop)
                view.textview_services.text = "ร้านสะดวกซื้อ"
            }
            "cctv" -> {
                view.imageview_icon.setImageResource(R.drawable.ic_cctv)
                view.textview_services.text = "กล้องวงจรปิด"
            }
            "wifi" -> {
                view.imageview_icon.setImageResource(R.drawable.ic_wifi)
                view.textview_services.text = "WI-FI"
            }
            "parking" -> {
                view.imageview_icon.setImageResource(R.drawable.ic_parking)
                view.textview_services.text = "ลานจอดรถ"
            }
            "bed" -> {
                view.imageview_icon.setImageResource(R.drawable.icon_bed)
                view.textview_services.text = "เตียงเดี่ยว "
            }
            "aircon" -> {
                view.imageview_icon.setImageResource(R.drawable.icon_air_conditioner)
                view.textview_services.text = "เครื่องปรับอากาศ "
            }
            "wifi_room" -> {
                view.imageview_icon.setImageResource(R.drawable.ic_wifi)
                view.textview_services.text = "Wi-Fi"
            }
            "cabletv" -> {
                view.imageview_icon.setImageResource(R.drawable.television)
                view.textview_services.text = "เคเบิลทีวี"
            }
            "table" -> {
                view.imageview_icon.setImageResource(R.drawable.icon_desk)
                view.textview_services.text = "โต๊ะอ่านหนังสือ"
            }
            "furniture" -> {
                view.imageview_icon.setImageResource(R.drawable.armchair)
                view.textview_services.text = "เฟอร์นิเจอร์"
            }
            "wardrobe" -> {
                view.imageview_icon.setImageResource(R.drawable.icon_closet)
                view.textview_services.text = "ตู้เสื้อผ้า"
            }
        }
        return view
    }

}